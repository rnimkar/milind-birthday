cmake_minimum_required(VERSION 3.10)
set(CMAKE_EXPORT_COMPILE_COMMANDS 1 )

project(milind VERSION 0.1)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED 1)
set(OpenGL_GL_PREFERENCE GLVND)


if (UNIX)
    add_executable(${PROJECT_NAME} src/geometry.cpp src/plane.cpp)

    target_sources(${PROJECT_NAME} PRIVATE src/linux_main.cpp)
    message(STATUS "Detected UNIX")
    if (APPLE)
        message(STATUS "Detected APPLE")
    else ()
    message(STATUS "Detected linux")

    # target_link_libraries(${PROJECT_NAME} glfw)
    # find_package(glfw3 3.3 REQUIRED)
    # target_link_libraries(${PROJECT_NAME} glfw)
    #
    # find_package(GLEW REQUIRED)
    # target_link_libraries(${PROJECT_NAME} GLEW)
    #
    find_package(OpenGL REQUIRED)
    target_link_libraries(${PROJECT_NAME} OpenGL::GL X11)
    
    endif ()
else ()
    message(STATUS "Detected windows")
    add_executable(${PROJECT_NAME} WIN32 src/geometry.cpp src/plane.cpp src/color.cpp src/journey.cpp src/TowerAndPerson.cpp src/ShaniwarWada.cpp src/smartbaord.cpp)

    target_sources(${PROJECT_NAME} PUBLIC src/windows_main.cpp)
    target_link_libraries(${PROJECT_NAME} user32.lib gdi32.lib Winmm.lib)
endif ()
# add include path
target_include_directories(${PROJECT_NAME} PUBLIC
    "include"
    )
set(GLFW_BUILD_DOCS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_TESTS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_EXAMPLES OFF CACHE BOOL "" FORCE)

