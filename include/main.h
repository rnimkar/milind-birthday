#ifndef MAIN_H
#define MAIN_H

#ifdef _WIN32
#include <Windows.h>
#include <gl/GL.h>
#elif defined(__linux__)
#include <GL/gl.h>
#endif
#include <math.h>

#define FPS 30

#define CAKE_END  150
#define COMP_END  300
#define SCENE_END 450
#define TRAIN_END 600
#define DREAM_END 750


extern int count;

#endif //! MAIN_H

