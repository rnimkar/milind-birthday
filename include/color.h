#ifndef COLOR_H
#define COLOR_H

#include <Windows.h>

#include <gl/GL.h>
typedef struct color
{
    float r;
    float g;
    float b;
    void print()
    {
        glColor3f(r / 256.0f, g / 256.0f, b / 256.0f);
    }
} color_t;

extern color_t BLACK;
extern color_t WHITE;
extern color_t RED;
extern color_t GREEN;
extern color_t BLUE;
extern color_t GREY;
extern color_t DARK_GREY;
extern color_t LIGHT_GREY;
extern color_t BACKGROUND;
extern color_t WARALI;
extern color_t CERTIFICATES_color_tS;
extern color_t YELLOW;
extern color_t VIOLET;
extern color_t VISTARA_PURPLE;
extern color_t VISTARA_YELLOW_OCHER;


#define color256(color) (glColor3f(color.r / 256.0f, color.g / 256.0f, color.b / 256.0f))

#endif // !COLOR_H

