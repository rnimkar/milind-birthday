#pragma once

#include "Geometry.h"
#include<windows.h>
#include<GL/GL.h>
#include"color.h"


void drawPlaneJourney(GLfloat x = 0.0, GLfloat y = 0.0);

GLfloat TranslatePlaneX(GLfloat n);

GLfloat TranslatePlaneY(GLfloat n, bool flag = false);

void drawGermany(GLfloat x);

void drawsea();

void drawStars();

void drawSun(GLfloat time);

void drawsky(color_t color[]);

void drawPlane(GLfloat x, GLfloat y);