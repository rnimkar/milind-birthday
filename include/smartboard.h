#ifndef SMARTBOARD_H
#define SMARTBOARD_H

#pragma once
#include "color.h"
#include "geometry.h"

/**
 * @brief Draw a rectangle
 *
 * @param center     [in] - center of smartboard
 * @param height     [in] - height of smartboard
 * @param width      [in] - breadth of smartboard
 * @param scale      [in] - scale/zoom
 * @param boardColor [in] - Back ground color of board
 */
void drawSmartBoard(point_t pCenter, float height, float width, float scale, color_t boardColor);

#endif