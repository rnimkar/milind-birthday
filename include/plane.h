#ifndef PLANE_H
#define PLANE_H
#include "geometry.h"
#include <GL/gl.h>

void drawPlane(const Point& center, GLfloat zoom);

#endif // !PLANE_H
