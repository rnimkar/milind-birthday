#ifndef GEOMETRY_H
#define GEOMETRY_H


#include "main.h"
#include <cmath>
#include <cstdint>
#include <cstdarg>

/**
 * @brief Value of PI
 */
const GLdouble PI = 2 * std::acos(0.0);

#define BALOON_Y_TRAVEL 0.25f

typedef struct Point
{
    float x;
    float y;
    float z;
    void print()
    {
        glVertex2f(x, y);
    }
} point_t;

struct CubicBezzier
{
    Point* start;
    Point* end;
    Point controlPoints[2];
    void at(GLfloat u, Point& outPoint);
    void draw();
};

struct QuadraticBezzier
{
    Point* start;
    Point* end;
    Point controlPoint;
    Point& at(GLfloat u, Point& outPoint);
    void drawAt(GLfloat u);
    void draw();
};
struct BezzierQuad
{
    CubicBezzier top;
    CubicBezzier bottom;
    void draw();
};

/**
 * @brief Get angle in radians
 *
 * @param degrees [in] - angle in degrees
 * @return value of angle in radians
 */
inline GLdouble getRadian(int degrees) { return ((PI / 180) * degrees); }

/**
 * @brief Draw a circle
 *
 * @param pCenter [in] - pointer to center of circle
 * @param radius  [in] - radius of circle
 * @param scale   [in] - scale/zoom
 */
void drawCircle(const Point* pCenter, float radius, float scale);

/**
 * @brief Draw an arc
 *
 * @param radius     [in] - Radius of circle
 * @param pCenter    [in] - pointer to the center of the circle
 * @param angleStart [in] - Start angle of the arc
 * @param anleEnd    [in] - end angle of the arc
 * @param scale      [in] - scale/zoom magnification
 */
void drawArc(float radius, const point_t* pCenter, uint32_t angleStart, uint32_t anleEnd, float scale);

/**
 * @brief Draw a rectangle
 *
 * @param center  [in] - center of rectangle
 * @param length  [in] - height of rectangle
 * @param breadth [in] - breadth of rectangle
 * @param scale   [in] - scale/zoom
 */
void drawRectangle(point_t pCenter, float length, float breadth, float scale);

/**
 * @brief Draw an equilateral triangle
 *
 * @param pCenter [in] - pointer to the center of the triangle
 * @param radius  [in] - distance of point from the center of the circle
 * @param scale   [in] - scale/zoom
 */
void drawEquilateralTriangle(const Point& pCenter, const float radius, const float scale);
void drawPolygon(float zoom, int verticesCount, ...);

void drawLine(const Point* pOrigin, Point* pEnd, float scale);

void drawLineLoop(float zoom, int verticesCount, ...);

void drawEllipse(const point_t* pCenter, float rx, float ry, int num_segments);
void displayGrid();

void drawVerticalStabilizer();
void drawHorizontalStabilizers();
void drawWings();
void drawBAckHorizontalStabilizer();
    void drawWindShield();
    void drawTriangle(float zoom, point_t point1, point_t point2, point_t point3);
#endif // !GEOMETRY_H

