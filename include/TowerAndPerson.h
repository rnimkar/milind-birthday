#ifndef TOWERANDPERSON_H
#define TOWERANDPERSON_H
#include "geometry.h"
#include <windows.h> // win32 api
#include <stdio.h> // For file io
#include <stdlib.h> // for exit
#include <GL/gl.h>
#include <cmath>
#include <cstdint>
#include <cstdarg>


// ====================  Custom Function =============
void drawPoints5(point_t pCenter);
void drawline(point_t pOrigin, point_t pEnd, float scale);
void screenbackground(Point pCenter);
void drawcloud(Point pCenter);
void drawtower(Point pCenter);
void grassbackground(Point pCenter);
void drawPerson(Point pCenter);
#endif 
