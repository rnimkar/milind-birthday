#include "geometry.h"
#include "plane.h"


#define PLANE_START -0.8f

#define NOSE_LENGTH 0.05
#define NOSE_HEIGHT_FRONT 0.1
#define NOSE_HEIGHT_REAR 0.2

#define CABIN_START (PLANE_START + NOSE_LENGTH)
#define CABIN_LENGTH 0.1
#define CABIN_END (CABIN_START + CABIN_LENGTH)

#define FUSELAGE_HEIGHT 0.2f
#define WING_START (CABIN_END + 0.2f)
#define WING_WIDTH 0.1f
#define WING_END (WING_START + WING_WIDTH)
#define TAIL_START (WING_END + 0.25f)
#define TAIL_LENGTH_BASE 0.2f
#define TAIL_LENGTH_TOP 0.15f
#define TAIL_END (TAIL_START + TAIL_LENGTH_BASE)
#define TAIL_HEIGHT 0.2f
#define APU_LENGTH 0.05f
#define PLANE_END (TAIL_END + APU_LENGTH)

/* Plane points definition */
Point windowStart       = { PLANE_START + NOSE_LENGTH, 0.03f };
Point cabinTop          = { CABIN_END, FUSELAGE_HEIGHT / 2 };
Point cabinBottom       = { CABIN_END, -FUSELAGE_HEIGHT / 2 };
Point tailBase          = { TAIL_START, FUSELAGE_HEIGHT / 2 - 0.025 };
Point bottomFuselaseEnd = { TAIL_START, -FUSELAGE_HEIGHT / 4 };
Point apuEnd            = { TAIL_END, tailBase.y };
Point tailTopRight      = { apuEnd.x + 0.05f, cabinTop.y + TAIL_HEIGHT };
/* Plane point definition ends here */

#ifndef INCREMENT
#define INCREMENT 0.01
#endif // !INCREMENT

CubicBezzier nose = {
    &cabinBottom, &windowStart,
    {
        { PLANE_START - 0.03, cabinBottom.y },
        { PLANE_START - 0.05, 0.01f },
    }
};

CubicBezzier cabin = {
    &windowStart, &cabinTop,
    {
        { windowStart.x + 0.01f, cabinTop.y - 0.025f },
        { windowStart.x + 0.025f, cabinTop.y },
    }
};

CubicBezzier fuselase{
    &cabinTop, &apuEnd, {{ cabinTop.x + 0.05f, cabinTop.y + 0.01f }, { tailBase.x - 0.05f, tailBase.y + 0.01f }}
};

CubicBezzier bottomFuselase{
    &cabinBottom, &bottomFuselaseEnd, {{ bottomFuselaseEnd.x - 0.15f, cabinBottom.y }, { bottomFuselaseEnd.x - 0.2f, cabinBottom.y }}
};

CubicBezzier apu{
    &apuEnd, &bottomFuselaseEnd,
    {
                                { apuEnd.x, apuEnd.y - 0.05f },
                                { apuEnd.x, apuEnd.y - 0.05f },
                                }
};

void drawPlane(const Point& center, GLfloat zoom)
{
    glColor3f(0.80f, 0.80f, 0.80f);
    drawHorizontalStabilizers();
    glLineWidth(2);
    glBegin(GL_POLYGON);
    // glPointSize(2.0f);
    glColor3f(0.92f, 0.92f, 0.92f);
    nose.draw();
    cabin.draw();
    fuselase.draw();
    apu.draw();
    bottomFuselase.draw();
    glEnd();
    glColor3f(0.80f, 0.80f, 0.80f);
    drawVerticalStabilizer();
    drawBAckHorizontalStabilizer();
    drawWings();
    drawWindShield();
}
void drawVerticalStabilizer()
{
    Point& topRight    = tailTopRight;
    Point  topLeft     = { topRight.x - 0.05f, topRight.y };
    Point& bottomLeft  = tailBase;
    Point  bottomRight = { TAIL_END - 0.04, apuEnd.y };
    // glColor3f(1.0f, 1.4f, 0.1f);
    glBegin(GL_POLYGON);
    topRight.print();
    topLeft.print();
    bottomLeft.print();
    bottomRight.print();
    glEnd();
}
void drawBAckHorizontalStabilizer()
{
    Point            bottomRight = { apuEnd.x + 0.07f, bottomFuselaseEnd.y };
    Point            bottomLeft  = { apuEnd.x + 0.02f, bottomRight.y };
    Point            topLeft     = { tailBase.x + 0.05f, tailBase.y - 0.05f };
    Point            topRight    = { apuEnd.x - 0.05f, topLeft.y };
    QuadraticBezzier top         = {
        &topLeft, &topRight, {topLeft.x + 0.05f, topLeft.y + 0.05f}
    };

    glBegin(GL_POLYGON);
    // glColor3f(4.0f, 0.0f, 1.0f);
    top.draw();
    // glColor3f(1.0f, 0.0f, 1.0f);
    topRight.print();
    bottomRight.print();
    bottomLeft.print();
    topLeft.print();

    glEnd();
}

void drawHorizontalStabilizers()
{
    // front
    Point            bottomRight = { apuEnd.x + 0.07f, bottomFuselaseEnd.y, -1.0f };
    Point            bottomLeft  = { apuEnd.x + 0.02f, bottomRight.y, -1.0f };
    Point            topLeft     = { tailBase.x + 0.05f, tailBase.y - 0.05f, -1.0f };
    Point            topRight    = { apuEnd.x - 0.05f, topLeft.y, -1.0f };
    QuadraticBezzier top         = {
        &topLeft, &topRight, {topLeft.x + 0.05f, topLeft.y + 0.05f}
    };

    // back
    Point backTopLeft  = { tailBase.x + 0.04f, tailBase.y + 0.15f, 1.0f };
    Point backTopRight = { backTopLeft.x + 0.04f, backTopLeft.y, 1.0f };
    topRight.z         = 1.0f;
    topLeft.z          = 1.0f;
    glBegin(GL_POLYGON);
    // glColor3f(0.2f, 0.5f, 1.0f);
    top.draw();
    topRight.print();
    backTopRight.print();
    backTopLeft.print();
    topLeft.print();

    glEnd();
}

void drawWings()
{
    // back
    Point topLeft     = { WING_START + 0.05f, cabinTop.y + TAIL_HEIGHT };
    Point topRight    = { topLeft.x + 0.04f, topLeft.y + 0.02f };
    Point bottomLeft  = { WING_START, cabinTop.y };
    Point bottomRight = { WING_END - 0.015f, cabinTop.y };
    glBegin(GL_POLYGON);
    topRight.print();
    bottomRight.print();
    bottomLeft.print();
    topLeft.print();

    glEnd();

    Point        frontBottomRight = { bottomFuselaseEnd.x, cabinBottom.y - 0.5f, 1.0f };
    Point        frontTopLeft     = { WING_START, cabinBottom.y + 0.02f, 1.0f };
    Point        frontTopRight    = { WING_END + 0.05f, cabinBottom.y + 0.01f, 1.0f };
    CubicBezzier wingTop          = {
        &frontTopLeft, &frontTopRight,
        {
                                      { frontTopLeft.x + 0.01f, frontTopLeft.y + 0.01f, frontTopLeft.z },
                                      { frontTopLeft.x + 0.02f, frontTopLeft.y + 0.03f, frontTopLeft.z },
                                      }
    };

    glBegin(GL_POLYGON);
    wingTop.draw();
    BezzierQuad frontWing = {

        {&frontTopRight, &frontBottomRight,
         {
                { frontTopRight.x + 0.09f, cabinBottom.y - 0.3f },
                { frontTopRight.x - 0.02f, cabinBottom.y - 0.018f },
            }},
        { &frontTopLeft, &frontBottomRight,
         {
                { frontBottomRight.x - 0.02f, frontBottomRight.y + 0.01f },
                { frontBottomRight.x - 0.02f, frontBottomRight.y - 0.025f },
            }}
    };
    frontWing.draw();
    glEnd();
}

void drawWindShield()
{
    Point       windShieldTopLeft     = { windowStart.x, windowStart.y };
    Point       windShieldBottomLeft  = { windowStart.x + 0.05f, windowStart.y - 0.05f };
    Point       windShieldTopRight    = { windowStart.x + 0.01f, windowStart.y + 0.03f };
    Point       windShieldBottomRight = { windowStart.x + 0.05f, windShieldBottomLeft.y + 0.05f };
    BezzierQuad windshield            = {

        {&windShieldTopRight, &windShieldBottomRight,
         {
                { windShieldTopRight.x + 0.002f, windShieldBottomRight.y + 0.010f },
                { windShieldTopRight.x + 0.006f, windShieldBottomRight.y + 0.006f },
            }},

        { &windShieldTopLeft, &windShieldBottomLeft,
         {
                { windShieldTopLeft.x + 0.004f, windShieldBottomLeft.y + 0.012f },
                { windShieldTopLeft.x + 0.008f, windShieldBottomLeft.y + 0.008f },
            }}
    };
    glColor3f(0.0f, 0.0f, 0.0f);
    glBegin(GL_POLYGON);
    windshield.draw();
    glEnd();
}
