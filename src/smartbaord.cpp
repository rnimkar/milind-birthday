#include<windows.h>
#include "smartboard.h"

void drawSmartBoard(point_t pCenter, float height, float width, float scale, color_t boardColor)
{
    //Variables
    point_t pCenterWhiteRect = {pCenter.x, pCenter.y - 0.45f};
    point_t pLeftWhiteRect   = {pCenter.x - 0.55f, pCenter.y - 0.45f};
    point_t pRightWhiteRect = {pCenter.x + 0.55f, pCenter.y - 0.45f};
    point_t pRightCircle = {pCenter.x + 0.675f, pCenter.y};
    point_t pLeftCircle = {pCenter.x - 0.675f, pCenter.y};
    point_t pBottomCircle = {pCenter.x, pCenter.y - 0.56f};
    point_t pVioletRect = {pCenter.x - 0.25f, pCenter.y - 0.45f};
    point_t pLeftPiller = {pCenter.x - (width + 0.3f), pCenter.y};
     point_t pRightPiller = {pCenter.x + width, pCenter.y};


    int ScreenWidth, ScreenHeight;
    ScreenWidth  =  GetSystemMetrics(SM_CXSCREEN);
    ScreenHeight =  GetSystemMetrics(SM_CYSCREEN);
    //Code
    color256(DARK_GREY);
    drawRectangle(pCenter, height, width, scale);

    color256(BLACK);
    drawRectangle(pCenter, height, width, scale - 0.03);

    color256(boardColor);
    drawRectangle(pCenter, height, width, scale - 0.05);

    WHITE.print();
    drawRectangle(pCenterWhiteRect, height + 4 , width - 1, scale - 0.4f);
    drawRectangle(pLeftWhiteRect, height - 1  , width - 1, scale - 0.4f);
    drawRectangle(pRightWhiteRect, height - 1  , width - 1, scale - 0.4f);
    drawArc(0.04f, &pRightCircle,90,272,1.0f);
    drawArc(0.04f, &pLeftCircle,270,452,1.0f);
    drawArc(0.04f, &pBottomCircle,0,180,1.0f);

    color256(VIOLET);
    drawRectangle(pVioletRect, height - 2 , width - 1  , scale - 0.4f);

    color256(DARK_GREY);
   drawRectangle(pLeftPiller, height, width , scale);




}
