#include "TowerAndPerson.h"
#include "geometry.h"
#include <math.h>
#include <windows.h> // win32 api
#include <GL/gl.h>
#include <stdio.h> // For file io
#include <stdlib.h> // for exit


void drawPoints5(point_t pCenter) {
	glLoadIdentity();
	glPointSize(5.5f);
	glColor3f(1, 1, 1);
	glBegin(GL_POINTS);
	glVertex3f(pCenter.x, pCenter.y, 0.0f);
	glEnd();
}

void drawline(point_t pOrigin, point_t pEnd, float scale)
{
	glBegin(GL_LINES);
	glVertex3f(pOrigin.x * scale, pOrigin.y * scale, pOrigin.z);
	glVertex3f(pEnd.x * scale, pEnd.y * scale, pEnd.z);
	glEnd();
}

void drawtower(Point pCenter)
{
	pCenter.y += 0.16;
	point_t pb1 = { pCenter.x + 0.0f, pCenter.y - 0.4f,0.8 };
	glColor3f(0.3515625, 0.3515625, 0.3515625);
	drawRectangle(pb1, 1, 25, 0.05);// main pole of tower


	glColor3f(1, 0, 0);
	point_t pb2 = { pCenter.x + 0.0f,pCenter.y - 0.15f };// Top Side  square

	glColor3f(1, 1, 1);
	drawRectangle(pb2, 1, 1, 0.09);// Middle square

	// Black Line for divide square
	glColor3f(0, 0, 0);
	Point pb21 = { pCenter.x - 0.09f,pCenter.y - 0.12f };
	Point pb22 = { pCenter.x + 0.08f,pCenter.y - 0.12f };
	drawline(pb21, pb22, 0.5);

	// Top 
	point_t pb4 = { pCenter.x + 0.0f,pCenter.y + 0.15f };
	glColor3f(1, 1, 1);
	drawRectangle(pb4, 5, 1, 0.02);
	// Red
	point_t pb5 = { pCenter.x + 0.0f,pCenter.y + 0.19f };
	glColor3f(1, 0, 0);
	drawRectangle(pb5, 1, 1, 0.07);
	//Anntena LINES

	glLineWidth(8.0f);
	glColor3f(1, 1, 1);
	Point pb53 = { pCenter.x + 0.00f,pCenter.y + 0.60f,0 };
	Point pb54 = { pCenter.x + 0.00f,pCenter.y + 1.24f,0 };
	drawline(pb53, pb54, 0.5);

	glLineWidth(1.2f);
	glColor3f(1, 0, 0);
	Point pb01 = { pCenter.x + 0.01f,pCenter.y + 0.5f,0 };
	Point pb02 = { pCenter.x - 0.01f,pCenter.y + 0.5f,0 };
	drawline(pb01, pb02, 0.5);

	Point pb11 = { pCenter.x + 0.01f,pCenter.y + 0.55f };
	Point pb12 = { pCenter.x - 0.01f,pCenter.y + 0.55f };
	drawline(pb11, pb12, 0.5);
	glColor3f(1, 0, 0);
	Point pb012 = { pCenter.x + 0.01f,pCenter.y + 0.6f };
	Point pb022 = { pCenter.x - 0.01f,pCenter.y + 0.6f };
	drawline(pb012, pb022, 0.5);

	glColor3f(1, 0, 0);
	Point pb013 = { pCenter.x + 0.01f, pCenter.y + 0.65f };
	Point pb023 = { pCenter.x - 0.01f,pCenter.y + 0.65f };
	drawline(pb013, pb023, 0.5);

	glColor3f(1, 0, 0);
	Point pb014 = { pCenter.x + 0.01f,pCenter.y + 0.70f };
	Point pb024 = { pCenter.x - 0.01f,pCenter.y + 0.70f };
	drawline(pb014, pb024, 0.5);

	glColor3f(1, 0, 0);
	Point pb015 = { pCenter.x + 0.01f,pCenter.y + 0.75f };
	Point pb025 = { pCenter.x - 0.01f,pCenter.y + 0.75f };
	drawline(pb015, pb025, 0.5);

	glColor3f(1, 0, 0);
	Point pb016 = { pCenter.x + 0.01f,pCenter.y + 0.79f };
	Point pb026 = { pCenter.x - 0.01f,pCenter.y + 0.79f };
	drawline(pb016, pb026, 0.5);

	glColor3f(1, 0, 0);
	Point pb017 = { pCenter.x + 0.01f,pCenter.y + 0.84f };
	Point pb027 = { pCenter.x - 0.01f,pCenter.y + 0.84f };
	drawline(pb017, pb027, 0.5);

	glColor3f(1, 0, 0);
	Point pb018 = { pCenter.x + 0.01f,pCenter.y + 0.88f };
	Point pb028 = { pCenter.x - 0.01f,pCenter.y + 0.88f };
	drawline(pb018, pb028, 0.5);

	glColor3f(1, 0, 0);
	Point pb019 = { pCenter.x + 0.01f,pCenter.y + 0.95f };
	Point pb029 = { pCenter.x - 0.01f,pCenter.y + 0.95f };
	drawline(pb019, pb029, 0.5);


	//Main Circle
	const Point circle = { pCenter.x + 0.0f,pCenter.y + 0.0f,0 };
	glColor3f(0, 0, 1);
	drawCircle(&circle, 0.8f, 0.1f);
	point_t wp1 = { pCenter.x + 0.0f,pCenter.y + 0.17f,0 };
	drawPoints5(wp1);
	point_t wp2 = { pCenter.x + 0.0f, pCenter.y + 0.0f,0 };
	drawPoints5(wp2);
	point_t wp3 = { pCenter.x - 0.02f,pCenter.y + 0.0f,0 };
	drawPoints5(wp3);
	point_t wp4 = { pCenter.x + 0.02f,pCenter.y + 0.0f,0 };
	drawPoints5(wp4);
	point_t wp5 = { pCenter.x + 0.04f, pCenter.y + 0.0f,0 };
	drawPoints5(wp5);
	point_t wp6 = { pCenter.x - 0.04f,pCenter.y + 0.0f,0 };
	drawPoints5(wp6);
	point_t wp7 = { pCenter.x + 0.06f,pCenter.y + 0.0f,0 };
	drawPoints5(wp7);
	point_t wp8 = { pCenter.x - 0.06f, pCenter.y + 0.0f,0 };
	drawPoints5(wp8);

	point_t wp11 = { pCenter.x + 0.01f,pCenter.y - 0.04f,0 };
	drawPoints5(wp11);
	point_t wp12 = { pCenter.x - 0.01f, pCenter.y - 0.04f,0 };
	drawPoints5(wp12);
	point_t wp13 = { pCenter.x + 0.03f,pCenter.y - 0.04f,0 };
	drawPoints5(wp13);
	point_t wp14 = { pCenter.x - 0.03f, pCenter.y - 0.04f,0 };
	drawPoints5(wp14);
	point_t wp15 = { pCenter.x + 0.05f,pCenter.y - 0.04f,0 };
	drawPoints5(wp15);
	point_t wp16 = { pCenter.x - 0.05f,pCenter.y - 0.04f,0 };
	drawPoints5(wp16);

	// Bottom trinagle
	glColor3f(0.3515625, 0.3515625, 0.3515625);
	const Point p21 = { pCenter.x + 0.0f,pCenter.y - 0.8f };

	drawEquilateralTriangle(p21, 1.2, 0.1);

}

void drawPerson(Point pCenter) {

	const Point pswh1 = { pCenter.x - 0.58f,pCenter.y - 0.29f,0.0f };// womain hair
	const Point pswh01 = { pCenter.x - 0.60f,pCenter.y - 0.31f,0.0f };
	const Point pswh02 = { pCenter.x - 0.56f,pCenter.y - 0.31f,0.0f };

	const Point psmh2 = { pCenter.x - 0.80f,pCenter.y - 0.26f,0.0f }; // man hair 
	const Point psch01 = { pCenter.x - 0.70f,pCenter.y - 0.4f,0.0f };// child hair

	glColor3f(0, 0, 0);
	drawCircle(&pswh1, 0.6f, 0.1f);
	drawEquilateralTriangle(pswh01, 1.2, 0.06);
	drawEquilateralTriangle(pswh02, 1.2, 0.06);
	glColor3f(0, 0, 0);
	drawCircle(&psmh2, 0.6f, 0.1f);

	Point pswd01 = { pCenter.x - 0.58f,pCenter.y - 0.46f };// women dress
	Point pswn01 = { pCenter.x - 0.58f,pCenter.y - 0.37f };// women neck
	Point psmn01 = { pCenter.x - 0.81f,pCenter.y - 0.33f }; // man neck
	Point psmd01 = { pCenter.x - 0.81f,pCenter.y - 0.43f };// man dress
	Point pscc01 = { pCenter.x - 0.70f,pCenter.y - 0.50f }; // child cloth
	Point pscn01 = { pCenter.x - 0.70f,pCenter.y - 0.44f };

	const Point psmh01 = { pCenter.x - 0.83f,pCenter.y - 0.375f,0.0f };// sholder man
	const Point psmh02 = { pCenter.x - 0.79f,pCenter.y - 0.375f,0.0f }; // sholder man

	const Point pswh12 = { pCenter.x - 0.605f,pCenter.y - 0.405f,0.0f };// sholder woman
	const Point pswh11 = { pCenter.x - 0.555f,pCenter.y - 0.405f,0.0f }; // sholder woman
	glColor3f(0.90625, 0.7421875, 0.671875);
	drawRectangle(pswn01, 1, 2, 0.03); // neck women
	glColor3f(1, 0, 0.75390625); // women dress color 

	drawCircle(&pswh11, 0.4f, 0.12f);
	drawCircle(&pswh12, 0.4f, 0.12f);
	drawRectangle(pswd01, 1, 2, 0.1); // women dress 

	glColor3f(0.90625, 0.7421875, 0.671875);

	drawRectangle(psmn01, 1, 1, 0.03); // neck man




	glColor3f(0, 0.18359375, 0.421875);
	drawCircle(&psmh01, 0.4f, 0.12f);
	drawCircle(&psmh02, 0.4f, 0.12f);
	drawRectangle(psmd01, 1, 2, 0.1);// man shirt





	glColor3f(0, 0, 0);
	drawCircle(&psch01, 0.6f, 0.06f);// Boy hair

	glColor3f(0.90625, 0.7421875, 0.671875); // BOY neck
	drawRectangle(pscn01, 1, 3, 0.008);// boy neck
	glColor3f(1, 0.0, 0.0); // BOY dress color 
	const Point psch1 = { pCenter.x - 0.715f,pCenter.y - 0.475f,0.0f }; // sholder child
	const Point psch2 = { pCenter.x - 0.685f,pCenter.y - 0.475f,0.0f };// shoulder child
	drawRectangle(pscc01, 1, 2, 0.05);// boy cloth

	drawCircle(&psch1, 0.2f, 0.12f);//
	drawCircle(&psch2, 0.2f, 0.12f);//

	// draw beanch
	glColor3f(0.8515625, 0.50390625, 0.23828125);
	Point beanch01 = { pCenter.x - 0.70f,pCenter.y - 0.55f };
	drawRectangle(beanch01, 10, 1, 0.05);
	Point beanch02 = { pCenter.x - 0.5f,pCenter.y - 0.65f };
	drawRectangle(beanch02, 1, 4, 0.05);
	Point beanch03 = { pCenter.x - 0.9f,pCenter.y - 0.65f };
	drawRectangle(beanch03, 1, 4, 0.05);


}
void drawcloud(Point pCenter)
{
	const Point ps1 = { pCenter.x + 0.38f,pCenter.y + 0.66f,0.0f };
	glColor3f(1, 1, 1);
	drawCircle(&ps1, 0.6f, 0.18f);
	const Point ps2 = { pCenter.x + 0.25f,pCenter.y + 0.6f,0 };
	glColor3f(1, 1, 1);
	drawCircle(&ps2, 0.6f, 0.18f);
	const Point ps3 = { pCenter.x + 0.52f,pCenter.y + 0.6f,0 };
	glColor3f(1, 1, 1);
	drawCircle(&ps3, 0.6f, 0.18f);
	glLoadIdentity();
	glColor3f(1, 1, 1);
	point_t p2 = { pCenter.x + 0.4f,pCenter.y + 0.55f,0 };
	drawRectangle(p2, 3, 1, 0.09);

	const Point p2s1 = { pCenter.x - 0.4f,pCenter.y + 0.5f ,0.0 };
	glColor3f(1, 1, 1);
	drawCircle(&p2s1, 0.5f, 0.2f);
	const	Point p2s2 = { pCenter.x - 0.45f,pCenter.y + 0.5f,0 };
	const Point p2s3 = { pCenter.x - 0.52f,pCenter.y + 0.65f,0 };
	glColor3f(1, 1, 1);

	drawCircle(&p2s3, 0.45f, 0.2f);
	glColor3f(1, 1, 1);
	drawCircle(&p2s2, 0.5f, 0.4f);


}



void screenbackground(Point pCenter)
{
	glColor3f(0.52734375f, 0.8046875f, 0.91796875f); // SKY color
	point_t p1 = { pCenter.x + 0.0f,pCenter.y + 0.5f,0 };
	drawRectangle(p1, 5, 5, 0.9);// background
}
void grassbackground(Point pCenter)
{
	// Green 
	point_t p22 = { pCenter.x + 0.0f,pCenter.y - 0.9f };
	glColor3f(0, 0.9f, 0);
	drawRectangle(p22, 20, 6, 0.1);
}
