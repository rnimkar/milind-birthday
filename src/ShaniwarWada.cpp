#include <windows.h>
#include <GL/gl.h>
#include "geometry.h"

point_t pCenter, pCenter1, pCenter2, pCenter3, pCenter4;

void drawShaniwarWada(void)
{
    // Function Declarations
    void drawWindow();
    void drawBase();
    void drawRoof();
    void drawWindowStrips();
    void drawStairs();

    // Code
    glClear(GL_COLOR_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // Shaniwar Wada Background
    glColor3f(0.52734375f, 0.8046875f, 0.91796875f);
    pCenter.x = 0.0f;
    pCenter.y = 0.3f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 2, 2, 1.0f);

    glColor3f(0, 0.9f, 0);
    pCenter.y = -0.9f;
    drawRectangle(pCenter, 2, 0.4, 4.2f);

    glColor3f(0.824f, 0.706f, 0.549f);
    pCenter.x = 0.0f;
    pCenter.y = 0.3f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.35f, 0.7f, 1.0f);

    pCenter.x = 0.0f;
    pCenter.y = 0.7f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.4f, 0.25f, 1.0f);

    // Shaniwar Wada Left Side Area

    pCenter.x = -0.55f;
    pCenter.y = 0.25f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.2f, 0.6f, 1.0f);

    pCenter.x = -0.65f;
    pCenter.y = 0.56f;
    pCenter.z = 0.0f;

    pCenter1.x = -0.61f;
    pCenter1.y = 0.65f;
    pCenter1.z = 0.0f;

    pCenter2.x = -0.45f;
    pCenter2.y = 0.65f;
    pCenter2.z = 0.0f;

    pCenter3.x = -0.45f;
    pCenter3.y = 0.56f;
    pCenter3.z = 0.0f;
    drawPolygon(1.0f, 4, pCenter, pCenter1, pCenter2, pCenter3);

    glColor3f(0.961f, 0.871f, 0.702f);
    // pCenter.x = -0.4f;
    // pCenter.y = 0.3f;
    // pCenter.z = 0.0f;
    // drawRectangle(pCenter, 0.1f, 0.7f, 1.0f);

    // glColor3f(0.0f, 1.0f, 0.0f);
    pCenter.x = -0.48f;
    pCenter.y = 0.65f;
    pCenter.z = 0.0f;

    pCenter1.x = -0.46f;
    pCenter1.y = 0.67f;
    pCenter1.z = 0.0f;

    pCenter2.x = -0.35f;
    pCenter2.y = 0.65f;
    pCenter2.z = 0.0f;

    pCenter3.x = -0.35f;
    pCenter3.y = -0.05f;
    pCenter3.z = 0.0f;

    pCenter4.x = -0.48f;
    pCenter4.y = -0.05f;
    pCenter4.z = 0.0f;
    drawPolygon(1.0f, 5, pCenter, pCenter1, pCenter2, pCenter3, pCenter4);

    pCenter.x = -0.16f;
    pCenter.y = 0.65f;
    pCenter.z = 0.0f;

    pCenter1.x = -0.18f;
    pCenter1.y = 0.67f;
    pCenter1.z = 0.0f;

    pCenter2.x = -0.28f;
    pCenter2.y = 0.65f;
    pCenter2.z = 0.0f;

    pCenter3.x = -0.28f;
    pCenter3.y = -0.05f;
    pCenter3.z = 0.0f;

    pCenter4.x = -0.16f;
    pCenter4.y = -0.05f;
    pCenter4.z = 0.0f;
    drawPolygon(1.0f, 5, pCenter, pCenter1, pCenter2, pCenter3, pCenter4);

    // glColor3f(0.961f, 0.871f, 0.702f);
    // pCenter.x = -0.2f;
    // pCenter.y = 0.3f;
    // pCenter.z = 0.0f;
    // drawRectangle(pCenter, 0.1f, 0.7f, 1.0f);

    // glColor3f(0.871f, 0.722f, 0.529f);
    glColor3f(0.824f, 0.706f, 0.549f);
    pCenter.x = -0.32f;
    pCenter.y = 0.3f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.1f, 0.7f, 1.0f);

    // pCenter.x = 0.3f;
    // pCenter.y = 0.3f;
    // pCenter.z = 0.0f;
    // drawRectangle(pCenter, 0.3f, 0.7f, 1.0f);

    // Shaniwar Wada Right Side Area
    pCenter.x = 0.55f;
    pCenter.y = 0.25f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.2f, 0.6f, 1.0f);

    pCenter.x = 0.65f;
    pCenter.y = 0.56f;
    pCenter.z = 0.0f;

    pCenter1.x = 0.61f;
    pCenter1.y = 0.65f;
    pCenter1.z = 0.0f;

    pCenter2.x = 0.45f;
    pCenter2.y = 0.65f;
    pCenter2.z = 0.0f;

    pCenter3.x = 0.45f;
    pCenter3.y = 0.56f;
    pCenter3.z = 0.0f;
    drawPolygon(1.0f, 4, pCenter, pCenter1, pCenter2, pCenter3);
    // glColor3f(0.961f, 0.871f, 0.702f);
    // pCenter.x = 0.4f;
    // pCenter.y = 0.3f;
    // pCenter.z = 1.0f;
    // drawRectangle(pCenter, 0.1f, 0.7f, 1.0f);

    // glColor3f(0.961f, 0.871f, 0.702f);
    // pCenter.x = 0.2f;
    // pCenter.y = 0.3f;
    // pCenter.z = 1.0f;
    // drawRectangle(pCenter, 0.1f, 0.7f, 1.0f);

    // glColor3f(0.871f, 0.722f, 0.529f);

    glColor3f(0.961f, 0.871f, 0.702f);
    pCenter.x = 0.48f;
    pCenter.y = 0.65f;
    pCenter.z = 0.0f;

    pCenter1.x = 0.46f;
    pCenter1.y = 0.67f;
    pCenter1.z = 0.0f;

    pCenter2.x = 0.35f;
    pCenter2.y = 0.65f;
    pCenter2.z = 0.0f;

    pCenter3.x = 0.35f;
    pCenter3.y = -0.05f;
    pCenter3.z = 0.0f;

    pCenter4.x = 0.48f;
    pCenter4.y = -0.05f;
    pCenter4.z = 0.0f;
    drawPolygon(1.0f, 5, pCenter, pCenter1, pCenter2, pCenter3, pCenter4);

    pCenter.x = 0.16f;
    pCenter.y = 0.65f;
    pCenter.z = 0.0f;

    pCenter1.x = 0.18f;
    pCenter1.y = 0.67f;
    pCenter1.z = 0.0f;

    pCenter2.x = 0.28f;
    pCenter2.y = 0.65f;
    pCenter2.z = 0.0f;

    pCenter3.x = 0.28f;
    pCenter3.y = -0.05f;
    pCenter3.z = 0.0f;

    pCenter4.x = 0.16f;
    pCenter4.y = -0.05f;
    pCenter4.z = 0.0f;
    drawPolygon(1.0f, 5, pCenter, pCenter1, pCenter2, pCenter3, pCenter4);

    glColor3f(0.824f, 0.706f, 0.549f);
    pCenter.x = 0.32f;
    pCenter.y = 0.3f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.1f, 0.7f, 1.0f);

    drawBase();
    drawWindowStrips();
    drawWindow();
    drawRoof();
    drawStairs();

    // glBegin(GL_TRIANGLES);

    // glColor3f(1.0f, 0.0f, 0.0f);
    // glVertex3f(0.0f, 1.0f, 0.0f);

    // glColor3f(0.0f, 1.0f, 0.0f);
    // glVertex3f(-1.0f, -1.0f, 0.0f);

    // glColor3f(0.0f, 0.0f, 1.0f);
    // glVertex3f(1.0f, -1.0f, 0.0f);

    // glEnd();
}

void drawStairs(void)
{
    glColor3f(1.000f, 0.871f, 0.678f);
    pCenter.x = -0.14f;
    pCenter.y = -0.17f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.035f, 0.23f, 1.0f);

    pCenter.x = 0.14f;
    drawRectangle(pCenter, 0.035f, 0.23f, 1.0f);

    glColor3f(0.722f, 0.525f, 0.043f);
    pCenter.x = -0.12f;
    pCenter.y = -0.17f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.025f, 0.23f, 1.0f);

    pCenter.x = 0.12f;
    drawRectangle(pCenter, 0.025f, 0.23f, 1.0f);

    glColor3f(1.000f, 0.871f, 0.678f);
    pCenter.x = 0.0f;
    pCenter.y = -0.17f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.22f, 0.04f, 1.0f);
    pCenter.y = -0.09f;
    drawRectangle(pCenter, 0.22f, 0.04f, 1.0f);

    glColor3f(0.824f, 0.706f, 0.549f);
    pCenter.y = -0.22f;
    drawRectangle(pCenter, 0.22f, 0.04f, 1.0f);
    glColor3f(1.000f, 0.871f, 0.678f);
    pCenter.y = -0.25f;
    drawRectangle(pCenter, 0.22f, 0.04f, 1.0f);
}

void drawBase(void)
{
    // Shaniwar Wada Base
    glColor3f(0.961f, 0.871f, 0.702f);
    pCenter.x = 0.0f;
    pCenter.y = -0.07f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 1.6f, 0.035f, 1.0f);

    glColor3f(0.698f, 0.133f, 0.133f);
    pCenter.y = -0.09f;
    drawRectangle(pCenter, 1.6f, 0.02f, 1.0f);

    glColor3f(0.824f, 0.706f, 0.549f);
    pCenter.x = 0.0f;
    pCenter.y = -0.15f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 1.6f, 0.1f, 1.0f);



    pCenter.x = 0.0f;
    pCenter.y = -0.15f;
    pCenter.z = 0.0f;

    // glColor3f(0.647f, 0.165f, 0.165f);
    // pCenter.x = -0.8f;
    // pCenter.y = -0.25f;
    // pCenter.z = 0.0f;

    // pCenter1.x = -0.75f;
    // pCenter1.y = 0.23f;
    // pCenter1.z = 0.0f;

    // pCenter2.x = 0.75f;
    // pCenter2.y = 0.23f;
    // pCenter2.z = 0.0f;

    // pCenter3.x = 0.8f;
    // pCenter3.y = 0.15f;
    // pCenter3.z = 0.0f;
    // drawPolygon(1.0f, 4, pCenter, pCenter1, pCenter2, pCenter3);
}

void drawRoof(void)
{
    // Shaniwar Wada Roof
    glColor3f(0.961f, 0.871f, 0.702f);
    // glColor3f(1.000f, 0.980f, 0.804f);
    pCenter.x = 0.0f;
    pCenter.y = 0.83f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.5f, 0.02f, 1.0f);

    glColor3f(0.627f, 0.322f, 0.176f);
    pCenter.x = 0.0f;
    pCenter.y = 0.555f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 1.3f, 0.01f, 1.0f);

    pCenter.x = 0.0f;
    pCenter.y = 0.645f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.97f, 0.01f, 1.0f);

    glColor3f(0.545f, 0.000f, 0.000f);
    pCenter.x = -0.25f;
    pCenter.y = 0.84f;
    pCenter.z = 0.0f;

    pCenter1.x = -0.15f;
    pCenter1.y = 0.9f;
    pCenter1.z = 0.0f;

    pCenter2.x = 0.15f;
    pCenter2.y = 0.9f;
    pCenter2.z = 0.0f;

    pCenter3.x = 0.25f;
    pCenter3.y = 0.84f;
    pCenter3.z = 0.0f;
    drawPolygon(1.0f, 4, pCenter, pCenter1, pCenter2, pCenter3);

    glColor3f(0.961f, 0.871f, 0.702f);
    pCenter.x = 0.0f;
    pCenter.y = 0.68f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.33f, 0.01f, 1.0f);

    glColor3f(0.804, 0.522, 0.247);
    pCenter.x = 0.0f;
    pCenter.y = 0.82f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.35f, 0.013f, 1.0f);

    pCenter.x = 0.0f;
    pCenter.y = 0.59f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.32f, 0.01f, 1.0f);

    pCenter.x = 0.0f;
    pCenter.y = 0.604f;
    pCenter.z = 0.0f;
    drawCircle(&pCenter, 0.009f, 1.0f);

    pCenter.x = 0.03f;
    drawCircle(&pCenter, 0.009f, 1.0f);

    pCenter.x = 0.06f;
    drawCircle(&pCenter, 0.009f, 1.0f);

    pCenter.x = 0.09f;
    drawCircle(&pCenter, 0.009f, 1.0f);

    pCenter.x = 0.12f;
    drawCircle(&pCenter, 0.009f, 1.0f);

    pCenter.x = 0.15f;
    drawCircle(&pCenter, 0.009f, 1.0f);

    pCenter.x = -0.03f;
    drawCircle(&pCenter, 0.009f, 1.0f);

    pCenter.x = -0.06f;
    drawCircle(&pCenter, 0.009f, 1.0f);

    pCenter.x = -0.09f;
    drawCircle(&pCenter, 0.009f, 1.0f);

    pCenter.x = -0.12f;
    drawCircle(&pCenter, 0.009f, 1.0f);

    pCenter.x = -0.15f;
    drawCircle(&pCenter, 0.009f, 1.0f);
}

void drawWindowStrips(void)
{
  
    // Shaniwar Wada Window Strips
    glColor3f(0.722f, 0.525f, 0.043f);
    pCenter.x = 0.57f;
    pCenter.y = 0.42f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.1f, 1.0f);

    pCenter.x = 0.61f;
    pCenter.y = 0.42f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.1f, 1.0f);

    pCenter.x = 0.53f;
    pCenter.y = 0.42f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.1f, 1.0f);

    pCenter.x = -0.57f;
    pCenter.y = 0.42f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.1f, 1.0f);

    pCenter.x = -0.61f;
    pCenter.y = 0.42f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.1f, 1.0f);

    pCenter.x = -0.53f;
    pCenter.y = 0.42f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.1f, 1.0f);

    pCenter.x = 0.45f;
    pCenter.y = 0.42f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.1f, 1.0f);

    pCenter.x = 0.41f;
    pCenter.y = 0.42f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.1f, 1.0f);

    pCenter.x = 0.35f;
    pCenter.y = 0.42f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.1f, 1.0f);

    pCenter.x = 0.32f;
    pCenter.y = 0.42f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.1f, 1.0f);

    pCenter.x = 0.29f;
    pCenter.y = 0.42f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.1f, 1.0f);

    pCenter.x = 0.24f;
    pCenter.y = 0.42f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.1f, 1.0f);

    pCenter.x = 0.2f;
    pCenter.y = 0.42f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.1f, 1.0f);

    pCenter.x = -0.57f;
    pCenter.y = 0.42f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.1f, 1.0f);

    pCenter.x = -0.61f;
    pCenter.y = 0.42f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.1f, 1.0f);

    pCenter.x = -0.53f;
    pCenter.y = 0.42f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.1f, 1.0f);

    pCenter.x = -0.57f;
    pCenter.y = 0.42f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.1f, 1.0f);

    pCenter.x = -0.61f;
    pCenter.y = 0.42f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.1f, 1.0f);

    pCenter.x = -0.53f;
    pCenter.y = 0.42f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.1f, 1.0f);

    pCenter.x = -0.45f;
    pCenter.y = 0.42f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.1f, 1.0f);

    pCenter.x = -0.41f;
    pCenter.y = 0.42f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.1f, 1.0f);

    pCenter.x = -0.35f;
    pCenter.y = 0.42f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.1f, 1.0f);

    pCenter.x = -0.32f;
    pCenter.y = 0.42f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.1f, 1.0f);

    pCenter.x = -0.29f;
    pCenter.y = 0.42f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.1f, 1.0f);

    pCenter.x = -0.24f;
    pCenter.y = 0.42f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.1f, 1.0f);

    pCenter.x = -0.2f;
    pCenter.y = 0.42f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.1f, 1.0f);

    // Middle Door Strips
    glColor3f(0.647f, 0.165f, 0.165f);
    pCenter.x = 0.12f;
    pCenter.y = 0.44f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.06f, 1.0f);

    pCenter.x = 0.08f;
    pCenter.y = 0.44f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.06f, 1.0f);

    pCenter.x = 0.04f;
    pCenter.y = 0.44f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.06f, 1.0f);

    pCenter.x = 0.0f;
    pCenter.y = 0.44f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.06f, 1.0f);

    pCenter.x = -0.04f;
    pCenter.y = 0.44f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.06f, 1.0f);

    pCenter.x = -0.08f;
    pCenter.y = 0.44f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.06f, 1.0f);

    pCenter.x = -0.12f;
    pCenter.y = 0.44f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.008f, 0.06f, 1.0f);

    glColor3f(0.722f, 0.525f, 0.043f);
    pCenter.x = -0.11f;
    pCenter.y = 0.37f;
    pCenter.z = 0.0f;
    drawCircle(&pCenter, 0.012f, 1.0f);

    pCenter.x = 0.11f;
    pCenter.y = 0.37f;
    pCenter.z = 0.0f;
    drawCircle(&pCenter, 0.012f, 1.0f);

    glColor3f(0.647f, 0.165f, 0.165f);
    pCenter.x = -0.18f;
    pCenter.y = 0.6f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.005f, 0.055f, 1.0f);

    pCenter.x = -0.215f;
    pCenter.y = 0.59f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.025f, 0.045f, 1.0f);
    pCenter.y = 0.61f;
    drawCircle(&pCenter, 0.012f, 1.0f);

    pCenter.x = -0.25f;
    pCenter.y = 0.6f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.005f, 0.055f, 1.0f);

    glColor3f(0.647f, 0.165f, 0.165f);
    pCenter.x = 0.18f;
    pCenter.y = 0.6f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.005f, 0.055f, 1.0f);

    pCenter.x = 0.215f;
    pCenter.y = 0.59f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.025f, 0.045f, 1.0f);
    pCenter.y = 0.61f;
    drawCircle(&pCenter, 0.012f, 1.0f);

    pCenter.x = 0.25f;
    pCenter.y = 0.6f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.005f, 0.055f, 1.0f);

    pCenter.x = -0.28f;
    pCenter.y = 0.6f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.005f, 0.055f, 1.0f);

    pCenter.x = -0.315f;
    pCenter.y = 0.59f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.025f, 0.045f, 1.0f);
    pCenter.y = 0.61f;
    drawCircle(&pCenter, 0.012f, 1.0f);

    pCenter.x = -0.35f;
    pCenter.y = 0.6f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.005f, 0.055f, 1.0f);

    pCenter.x = 0.28f;
    pCenter.y = 0.6f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.005f, 0.055f, 1.0f);

    pCenter.x = 0.315f;
    pCenter.y = 0.59f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.025f, 0.045f, 1.0f);
    pCenter.y = 0.61f;
    drawCircle(&pCenter, 0.012f, 1.0f);

    pCenter.x = 0.35f;
    pCenter.y = 0.6f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.005f, 0.055f, 1.0f);

    pCenter.x = -0.38f;
    pCenter.y = 0.6f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.005f, 0.055f, 1.0f);

    pCenter.x = -0.415f;
    pCenter.y = 0.59f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.025f, 0.045f, 1.0f);
    pCenter.y = 0.61f;
    drawCircle(&pCenter, 0.012f, 1.0f);

    pCenter.x = -0.45f;
    pCenter.y = 0.6f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.005f, 0.055f, 1.0f);

    pCenter.x = 0.38f;
    pCenter.y = 0.6f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.005f, 0.055f, 1.0f);

    pCenter.x = 0.415f;
    pCenter.y = 0.59f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.025f, 0.045f, 1.0f);
    pCenter.y = 0.61f;
    drawCircle(&pCenter, 0.012f, 1.0f);

    pCenter.x = 0.45f;
    pCenter.y = 0.6f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.005f, 0.055f, 1.0f);
}

void drawWindow(void)
{
    // Code
    glColor3f(0.961f, 0.871f, 0.702f);
    point_t pCenter;
    pCenter.x = 0.0f;
    pCenter.y = 0.1f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.2f, 0.3f, 1.0f);

    pCenter.y = 0.24f;
    drawArc(0.1f, &pCenter, 0, 180, 1.0f);

    glColor3f(0.647f, 0.165f, 0.165f);
    pCenter.y = 0.03f;
    drawRectangle(pCenter, 0.14f, 0.16f, 1.0f);

    glColor3f(0.647f, 0.165f, 0.165f);
    pCenter.y = 0.16f;
    drawRectangle(pCenter, 0.125f, 0.15f, 1.0f);

    pCenter.y = 0.225f;
    drawArc(0.062f, &pCenter, 0, 180, 1.0f);

    pCenter.y = 0.71f;
    drawRectangle(pCenter, 0.05f, 0.14f, 1.0f);

    pCenter.y = 0.782f;
    drawArc(0.025f, &pCenter, 0, 180, 1.0f);

    pCenter.x = 0.07f;
    pCenter.y = 0.71f;
    drawRectangle(pCenter, 0.05f, 0.14f, 1.0f);

    pCenter.x = 0.07f;
    pCenter.y = 0.782f;
    drawArc(0.025f, &pCenter, 0, 180, 1.0f);

    pCenter.x = 0.14f;
    pCenter.y = 0.71f;
    drawRectangle(pCenter, 0.05f, 0.14f, 1.0f);

    pCenter.x = 0.14f;
    pCenter.y = 0.782f;
    drawArc(0.025f, &pCenter, 0, 180, 1.0f);

    pCenter.x = -0.07f;
    pCenter.y = 0.71f;
    pCenter.z = 0.0f;
    drawRectangle(pCenter, 0.05f, 0.14f, 1.0f);

    pCenter.x = -0.07f;
    pCenter.y = 0.782f;
    drawArc(0.025f, &pCenter, 0, 180, 1.0f);

    pCenter.x = -0.14f;
    pCenter.y = 0.71f;
    drawRectangle(pCenter, 0.05f, 0.14f, 1.0f);

    pCenter.x = -0.14f;
    pCenter.y = 0.782f;
    drawArc(0.025f, &pCenter, 0, 180, 1.0f);
}
