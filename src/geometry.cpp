#include "geometry.h"
#include <GL/gl.h>
#include <math.h>

/**
 * @brief Draw a circle
 *
 * @param pCenter [in] - pointer to center of circle
 * @param radius  [in] - radius of circle
 * @param scale   [in] - scale/zoom
 */
void drawCircle(const Point* pCenter, float radius, float scale)
{
    glBegin(GL_POLYGON);
    for (unsigned int idx = 0U; idx < 360U; idx++)
    {
        double x = pCenter->x + cos(getRadian(idx)) * radius * scale;
        double y = pCenter->y + sin(getRadian(idx)) * radius * scale;
        glVertex3f(x, y, pCenter->z);
    }
    glEnd();
}

/**
 * @brief Draw an arc
 *
 * @param radius     [in] - Radius of circle
 * @param pCenter    [in] - pointer to the center of the circle
 * @param angleStart [in] - Start angle of the arc
 * @param anleEnd    [in] - end angle of the arc
 * @param scale      [in] - scale/zoom magnification
 */
void drawArc(float radius, const point_t* pCenter, uint32_t angleStart, uint32_t anleEnd, float scale)
{
    glBegin(GL_POLYGON);
    for (unsigned int idx = angleStart; idx < anleEnd; idx++)
    {
        double x = pCenter->x + cos(getRadian(idx)) * radius * scale;
        double y = pCenter->y + sin(getRadian(idx)) * radius * scale;
        glVertex3f(x, y, pCenter->z);
    }
    glEnd();
}

/**
 * @brief Draw a rectangle
 *
 * @param center  [in] - center of rectangle
 * @param length  [in] - height of rectangle
 * @param breadth [in] - breadth of rectangle
 * @param scale   [in] - scale/zoom
 */
void drawRectangle(point_t pCenter, float length, float breadth, float scale)
{
    float offset_x = (length * scale) / 2.0f;
    float offset_y = (breadth * scale) / 2.0f;

    glBegin(GL_POLYGON);
    glVertex3f(pCenter.x + offset_x, pCenter.y + offset_y, pCenter.z);
    glVertex3f(pCenter.x + offset_x, pCenter.y - offset_y, pCenter.z);
    glVertex3f(pCenter.x - offset_x, pCenter.y - offset_y, pCenter.z);
    glVertex3f(pCenter.x - offset_x, pCenter.y + offset_y, pCenter.z);
    glEnd();
}

/**
 * @brief Draw an equilateral triangle
 *
 * @param pCenter [in] - pointer to the center of the triangle
 * @param radius  [in] - distance of point from the center of the circle
 * @param scale   [in] - scale/zoom
 */
void drawEquilateralTriangle(const Point& pCenter, const float radius, const float scale)
{
    glBegin(GL_TRIANGLES);
    glVertex3f(pCenter.x, pCenter.y + radius * scale, pCenter.z);                                                            // top vertex
    glVertex3f(pCenter.x - radius * cos(getRadian(30)) * scale, pCenter.y - radius * sin(getRadian(30)) * scale, pCenter.z); // left vertex
    glVertex3f(pCenter.x + radius * cos(getRadian(30)) * scale, pCenter.y - radius * sin(getRadian(30)) * scale, pCenter.z); // left vertex
    glEnd();
}

void drawPolygon(float zoom, int verticesCount, ...)
{
    // variable declarations
    va_list points_list;
    point_t point;

    // code
    va_start(points_list, verticesCount);
    glBegin(GL_POLYGON);
    while (verticesCount)
    {
        point = va_arg(points_list, point_t);
        glVertex3f(point.x * zoom, point.y * zoom, point.z);
        verticesCount--;
    }
    glEnd();
}

void drawLine(const Point* pOrigin, Point* pEnd, float scale)
{
    glBegin(GL_LINES);
    glVertex3f(pOrigin->x * scale, pOrigin->y * scale, pOrigin->z);
    glVertex3f(pEnd->x * scale, pEnd->y * scale, pOrigin->z);
    glEnd();
}

void drawLineLoop(float zoom, int verticesCount, ...)
{
    // variable declarations
    va_list points_list;
    point_t point;
    // code
    va_start(points_list, verticesCount);
    glBegin(GL_LINE_LOOP);
    while (verticesCount)
    {
        point = va_arg(points_list, point_t);
        glVertex2f(point.x * zoom, point.y * zoom);
        verticesCount--;
    }
    glEnd();
}

void drawEllipse(const point_t* pCenter, float rx, float ry, int num_segments)
{
    float theta = 2 * 3.1415926 / float(num_segments);
    float c     = cosf(theta); // precalculate the sine and cosine
    float s     = sinf(theta);
    float t;

    float start_angle = 1; // we start at angle = 0
    float y           = 0;

    glBegin(GL_POLYGON);
    for (int ii = 0; ii < num_segments; ii++)
    {
        // apply radius and offset
        glVertex2f(start_angle * rx + pCenter->x, y * ry + pCenter->y); // output vertex

        // apply the rotation matrix
        t           = start_angle;
        start_angle = c * start_angle - s * y;
        y           = s * t + c * y;
    }
    glEnd();
}

void CubicBezzier::at(GLfloat u, Point& outPoint)
{
    outPoint.x = pow(1 - u, 3) * start->x + 3 * u * pow(1 - u, 2) * controlPoints[0].x + 3 * pow(u, 2) * (1 - u) * controlPoints[1].x + pow(u, 3) * end->x;
    outPoint.y = pow(1 - u, 3) * start->y + 3 * u * pow(1 - u, 2) * controlPoints[0].y + 3 * pow(u, 2) * (1 - u) * controlPoints[1].y + pow(u, 3) * end->y;
    outPoint.z = start->z;
}

Point& QuadraticBezzier::at(GLfloat u, Point& outPoint)
{
    outPoint.x = pow(1 - u, 2) * start->x + 2 * u * (1 - u) * controlPoint.x + pow(u, 2) * end->x;
    outPoint.y = pow(1 - u, 2) * start->y + 2 * u * (1 - u) * controlPoint.y + pow(u, 2) * end->y;
    outPoint.z = start->z;
    return outPoint;
}

#define INCREMENT 0.0005
void BezzierQuad::draw()
{   
    Point p, p2, p3, p4;
    for (GLfloat t = 0.0f; t < 1.0f; t += INCREMENT)
    {
        top.at(t, p);
        p.print();
        bottom.at(t + INCREMENT, p3);
        p3.print();
        top.at(t, p2);
        p2.print();
        bottom.at(t + INCREMENT, p4);
        p4.print();
    }
}

void QuadraticBezzier::drawAt(GLfloat u)
{   
    Point p;
    at(u, p).print();
}

void QuadraticBezzier::draw()
{   
    Point p;
    for (GLfloat t = 0.0f; t < 1.0f; t += INCREMENT)
    {
        at(t, p);
        p.print();
    }
}
void CubicBezzier::draw()
{   
    Point p;
    for (GLfloat t = 0.0f; t < 1.0f; t += INCREMENT)
    {
        at(t, p);
        p.print();
    }
}

void displayGrid()
{
    glBegin(GL_LINES);
    glColor3f(0.1f, 0.1f, 0.1f);
    for(GLfloat i =-0.95f; i <= 1; i+=0.05)
    {
        glVertex3f(-1.0f, i, 1.0f);      
        glVertex3f(1.0f, i, 1.0f);      
        glVertex3f(i, -1.0f, 1.0f);      
        glVertex3f(i, 1.0f, 1.0f);      
    }
    glColor3f(1.0f, 1.0f, 1.0f);
        glVertex3f(-1.0f, 0.0f, 1.0f);      
        glVertex3f(1.0f, 0.0f, 1.0f);      
    glEnd();
}

void drawTriangle(float zoom, point_t point1, point_t point2, point_t point3)
{
    glBegin(GL_TRIANGLES);
    glVertex2f(point1.x * zoom, point1.y * zoom);
    glVertex2f(point2.x * zoom, point2.y * zoom);
    glVertex2f(point3.x * zoom, point3.y * zoom);
    glEnd();
}

