#include "geometry.h"
#include "color.h"


color_t BLACK = {0, 0, 0};
color_t WHITE = {255, 255, 255};
color_t RED = {255.0f, 0.0f, 0.0f};
color_t GREEN = {0.0f, 255.0f, 0.0f};
color_t BLUE = {0.0f, 0.0f, 255.0f};
color_t GREY = {128.0f, 128.0f, 128.0f};
color_t DARK_GREY = {64.0f, 64.0f, 64.0f};
color_t LIGHT_GREY = {192.0f, 192.0f, 192.0f};
color_t BACKGROUND = {216.0f, 208.0f, 194.0f};
color_t WARALI = {255.0f, 127.0f, 127.0f};
color_t CERTIFICATES_color_tS = {178.0f, 128.0f, 102.0f};
color_t YELLOW = {255.0f, 255.0f, 0.0f};
color_t VIOLET = {30.0f,60.0f,200.0f};
color_t VISTARA_PURPLE = {71, 20, 61};
color_t VISTARA_YELLOW_OCHER = {185, 151, 80};
