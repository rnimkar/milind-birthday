///headers
#include"journey.h"
#include"color.h"
#include"geometry.h"

bool g_color_selector = true;
bool g_isSkyOrange = true;
GLfloat g_Sun_ypos = 0;
int g_itime = 0;
bool g_bstartNight = false;

#define MULTIPLIER_1 0.01 
//#define MULTIPLIER_1 0.005
#define MULTIPLIER_2 0.002 

const GLfloat SUN_RISE = 10 * MULTIPLIER_1;
const GLfloat SUN_RISE_OVER = 300 * MULTIPLIER_1;
const GLfloat SUN_AT_TOP = 600 * MULTIPLIER_1;
const GLfloat SUN_SET = 800 * MULTIPLIER_1;
const GLfloat SUN_SET_OVER = 1600 * MULTIPLIER_1;
const GLfloat NIGHT_OVER = 2000 * MULTIPLIER_1;

color_t MT_BROWN_1{ 100,61,0 };
color_t MT_BROWN_2{ 58,36,0 };
color_t MT_ICE_1{ 250,250,250 };
color_t MT_ICE_2{ 234,234,234 };
color_t GL_GRASS_1{ 132,189,0 };
color_t GL_GRASS_2{ 55,76,7 };
color_t TREE_GREEN_1{ 40,145,17 };
color_t TREE_GREEN_2{ 35,106,4 };
color_t TREE_BROWN{ 94,57,1 };
color_t SEA_BLUE{ 2,96,165 };
color_t MT_R{ 235,32,17 };
color_t SUNRISE_COLORS_ORANGE{ 255,103,0 };
color_t SUNRISE_COLORS_YELLOW{ 255,167,0 };
color_t STAR_WHITE{ 256,256,256 };

void drawMountainType_1_At(GLfloat x = 0, GLfloat y = 0, bool color_selector = true)
{

	point_t mountain_1 = { -0.5f + x,0.30f + y };
	point_t mountain_2 = { -0.8f + x,-0.5f + y };
	point_t mountain_3 = { -0.2f + x,-0.5f + y };
	color256((color_selector ? MT_BROWN_1 : MT_BROWN_2));
	drawPolygon(0.70f, 3, mountain_1, mountain_2, mountain_3);

	point_t icecap_1 = { -0.5f + x,0.30f + y };//ice peaks
	point_t icecap_2 = { -0.56f + x,0.15f + y };
	point_t icecap_3 = { -0.44f + x,0.15f + y };
	color256((color_selector ? MT_ICE_1 : MT_ICE_2));
	drawPolygon(0.7f, 3, icecap_1, icecap_2, icecap_3);
}

void drawGrassland_At(GLfloat x = 0, bool color_selector = true)
{
	point_t grassland_1 = { -1.50f + x ,-0.10f };
	point_t grassland_2 = { 0.2f + x,-0.10f };

	point_t grassland_3 = { 0.2f + x,-0.2f };
	point_t grassland_4 = { 0.6f + x,-0.3f };
	point_t grassland_5 = { 0.65f + x ,-0.4f };
	point_t grassland_6 = { 0.7f + x,-0.5f };

	point_t grassland_7 = { 0.50f + x,-2.0f };
	point_t grassland_8 = { -2.0f + x,-2.0f };
	color256((color_selector ? GL_GRASS_1 : GL_GRASS_2));

	drawPolygon(0.70f, 8, grassland_1, grassland_2, grassland_3, grassland_4, grassland_5, grassland_6, grassland_7, grassland_8);
}

void drawTree(GLfloat x, GLfloat y, bool color_selector = true)
{
	GLfloat m = 0.06f;
	point_t tree_1 = { 0.5f * m + x,1.0f * m + y };//top
	point_t tree_2 = { 0.0f * m + x,-0.4f * m + y };//left
	point_t tree_3 = { 1.0f * m + x,-0.4f * m + y };//rigth
	color256((color_selector ? TREE_GREEN_1 : TREE_GREEN_2));
	drawTriangle(1.0f, tree_1, tree_2, tree_3);
	point_t tree_4 = { 0.5f * m + x,0.5f * m + y };
	point_t tree_5 = { 0.0f * m + x,-1.0f * m + y };
	point_t tree_6 = { 1.0f * m + x,-1.0f * m + y };
	color256((color_selector ? TREE_GREEN_1 : TREE_GREEN_2));
	drawTriangle(1.0f, tree_4, tree_5, tree_6);
	point_t tree_7 = { 0.5f * m + x,0.0f * m + y };
	point_t tree_8 = { 0.0f * m + x,-1.6f * m + y };
	point_t tree_9 = { 1.0f * m + x,-1.6f * m + y };
	color256((color_selector ? TREE_GREEN_1 : TREE_GREEN_2));
	drawTriangle(1.0f, tree_7, tree_8, tree_9);
	point_t tree_trunk1 = { 0.4f * m + x,-1.6f * m + y };
	point_t tree_trunk2 = { 0.4f * m + x, -2.0f * m + y };
	point_t tree_trunk3 = { 0.6f * m + x, -2.0f * m + y };
	point_t tree_trunk4 = { 0.6f * m + x,-1.6f * m + y };
	color256(TREE_BROWN);
	drawPolygon(1.0f, 4, tree_trunk1, tree_trunk2, tree_trunk3, tree_trunk4);

}

void drawsky(color_t color[])
{
	GLfloat zoom = 0.7f;
	point_t sky_1 = { -1.50f ,-0.30f };
	point_t sky_2 = { 0.5f ,-0.30f };

	point_t sky_3 = { 1.5f ,-0.30f };
	point_t sky_4 = { 1.5f ,1.5f };
	point_t sky_5 = { -1.5f ,1.5f };

	//color256(color[]);
	glBegin(GL_POLYGON);
	if (false == g_bstartNight)
	{
		(g_isSkyOrange ? SUNRISE_COLORS_YELLOW.print() : color[0].print());
		glVertex2f(sky_1.x * zoom, sky_1.y * zoom);
		(g_isSkyOrange ? SUNRISE_COLORS_ORANGE.print() : color[0].print());
		glVertex2f(sky_2.x * zoom, sky_2.y * zoom);
		(g_isSkyOrange ? SUNRISE_COLORS_YELLOW.print() : color[0].print());
		glVertex2f(sky_3.x * zoom, sky_3.y * zoom);
		color[0].print();
		glVertex2f(sky_4.x * zoom, sky_4.y * zoom);
		glVertex2f(sky_5.x * zoom, sky_5.y * zoom);
	}
	else
	{
		(g_itime * MULTIPLIER_1 < NIGHT_OVER ? color[1].print() : color[2].print());
		glVertex2f(sky_1.x * zoom, sky_1.y * zoom);
		glVertex2f(sky_2.x * zoom, sky_2.y * zoom);
		glVertex2f(sky_3.x * zoom, sky_3.y * zoom);
		glVertex2f(sky_4.x * zoom, sky_4.y * zoom);
		glVertex2f(sky_5.x * zoom, sky_5.y * zoom);
	}

	glEnd();
}

void drawsea()
{
	point_t sea_1 = { -1.50f ,-0.30f };
	point_t sea_2 = { 1.5f ,-0.30f };

	point_t sea_3 = { 1.5f ,-1.5f };
	point_t sea_4 = { -1.5f ,-1.5f };

	color256(SEA_BLUE);

	drawPolygon(0.70f, 4, sea_1, sea_2, sea_3, sea_4);
}

void drawGermany(GLfloat x = 0.0)
{
	void drawGrassland_At(GLfloat x = 0, bool color_selector = true);
	drawGrassland_At(x);

	void drawAllMountains(GLfloat x = 0);
	drawAllMountains(x);

	void drawAllTrees(GLfloat x = 0);
	drawAllTrees(x);
}

void drawAllMountains(GLfloat x = 0)
{

	void drawMountainType_1_At(GLfloat x = 0, GLfloat y = 0, bool color_selector = true);
	drawMountainType_1_At(0.6 + x, 0.1, false);
	drawMountainType_1_At(0.4 + x, 0.1, false);
	drawMountainType_1_At(0.1 + x, 0.1, false);
	drawMountainType_1_At(-0.2 + x, 0.1, false);
	drawMountainType_1_At(-0.4 + x, 0.1, false);
	drawMountainType_1_At(-0.6 + x, 0.1, false);
	drawMountainType_1_At(0.5 + x);
	drawMountainType_1_At(0.3 + x);
	drawMountainType_1_At(x);
	drawMountainType_1_At(-0.3 + x);
	drawMountainType_1_At(-0.5 + x);
	drawMountainType_1_At(-0.7 + x);
}

void drawAllTrees(GLfloat x = 0)
{
	void drawTree(GLfloat x, GLfloat y, bool color_selector = true);

	drawTree(0.2 + x, -0.4, false);
	drawTree(0.24 + x, -0.45);
	drawTree(0.16 + x, -0.45);

	drawTree(0.0 + x, -0.4);
	drawTree(0.04 + x, -0.45, false);
	drawTree(-0.06 + x, -0.45, false);

	drawTree(-0.2 + x, -0.5, false);
	drawTree(-0.24 + x, -0.55);
	drawTree(-0.16 + x, -0.55,false);

	drawTree(-0.4 + x, -0.4,false);
	drawTree(-0.44 + x, -0.45, false);
	drawTree(-0.36 + x, -0.45);

	drawTree(-0.5 + x, -0.5);
	drawTree(-0.54 + x, -0.55, false);
	drawTree(-0.46 + x, -0.55, false);
}



void drawSun(GLfloat time)
{
	if (g_itime * MULTIPLIER_1 > SUN_RISE && g_itime * MULTIPLIER_1 < SUN_RISE_OVER)
	{
		color256(SUNRISE_COLORS_ORANGE);
		g_isSkyOrange = true;
	}
	else if (g_itime * MULTIPLIER_1 > SUN_SET && g_itime * MULTIPLIER_1 < SUN_SET_OVER)
	{
		color256(SUNRISE_COLORS_ORANGE);
		g_isSkyOrange = true;
	}
	else
	{
		color256(SUNRISE_COLORS_YELLOW);
		g_isSkyOrange = false;
	}

	//color256(SUNRISE_COLORS_ORANGE);

	if (SUN_RISE < g_itime * MULTIPLIER_1 && g_itime * MULTIPLIER_1 < SUN_AT_TOP)
	{
		if (g_Sun_ypos < 0.8f)//sun top-level
			g_Sun_ypos += MULTIPLIER_2;
	}
	else if (g_itime * MULTIPLIER_1 > SUN_AT_TOP && SUN_SET_OVER > g_itime * MULTIPLIER_1)
	{
		if (g_Sun_ypos > -0.4f)//sun bottom-level
		{
			g_Sun_ypos -= MULTIPLIER_2;
		}
		else
		{
			g_bstartNight = true;
		}
	}


	point_t sun_center = { 0.5 ,g_Sun_ypos };

	drawCircle(&sun_center,0.7f, 0.2f );

}

void drawStars()
{
	color256(STAR_WHITE);
	glPointSize(3.0f);
	glBegin(GL_POINTS);
	glVertex2f(0.0f, 0.0f);
	glVertex2f(0.0f, 0.4f);
	glVertex2f(-0.3f, 0.4f);
	glVertex2f(-0.3f, 0.6f);
	glVertex2f(-0.5f, 0.8f);
	glVertex2f(-0.5f, 0.9f);
	glVertex2f(0.2f, 0.9f);
	glVertex2f(0.3f, 0.8f);
	glVertex2f(0.2f, 0.8f);
	glVertex2f(0.2f, 0.7f);
	glVertex2f(0.25f, 0.6f);
	glVertex2f(-0.3f, 0.8f);
	glVertex2f(-0.2f, 0.8f);
	glVertex2f(-0.2f, 0.7f);
	glVertex2f(-0.6f, 0.7f);
	glVertex2f(-0.5f, 0.7f);
	glVertex2f(-0.55f, 0.78f);
	glVertex2f(-0.4f, 0.65f);
	glVertex2f(-0.8f, 0.65f);
	glVertex2f(-0.9f, 0.75f);
	glVertex2f(-0.7f, 0.85f);
	glVertex2f(-0.75f, 0.50f);
	glVertex2f(-0.9f, 0.2f);
	glVertex2f(-0.8f, 0.25f);
	glVertex2f(-0.65f, 0.25f);
	glVertex2f(-0.7f, 0.25f);
	glVertex2f(-0.5f, 0.3f);
	glVertex2f(0.8f, 0.65f);
	glVertex2f(0.9f, 0.75f);
	glVertex2f(0.78f, 0.85f);
	glVertex2f(0.75f, 0.50f);
	glVertex2f(1.0f, 0.20f);
	glVertex2f(0.95f, 0.250f);
	glVertex2f(0.65f, 0.250f);
	glVertex2f(0.95f, 0.250f);
	glEnd();
}

void drawPlane(GLfloat x = 0, GLfloat y = 0)
{

	point_t dummyPlane_1 = { 0.10f + x,0.20f + y };
	point_t dummyPlane_2 = { 0.50f + x,0.20f + y };
	point_t dummyPlane_3 = { 0.40f + x,0.30f + y };
	point_t dummyPlane_4 = { 0.30f + x,0.30f + y };
	point_t dummyPlane_5 = { 0.20f + x,0.30f + y };
	point_t dummyPlane_6 = { 0.10f + x,0.40f + y };

	color256(STAR_WHITE);
	drawPolygon(0.70f, 6, dummyPlane_1, dummyPlane_2, dummyPlane_3, dummyPlane_4, dummyPlane_5, dummyPlane_6);
}

GLfloat TranslatePlaneY(GLfloat n, bool flag)
{
	GLfloat min_y = 0;
	GLfloat max_y = 0;
	if (flag)
	{
		min_y = 1; //-0.5
		max_y = -0.5;// 1.0
	}
	else
	{
		min_y = -0.5;
		max_y = 1.0;
	}

	GLfloat min_t = 0.005;
	GLfloat max_t = 5;

	return ((n - 0.005) / (max_t - min_t)) * (max_y - min_y) + min_y;
}

void drawPlaneJourney(GLfloat x, GLfloat y)
{

	//glPushMatrix();
	//glRotatef(angle, 1.0f, 1.0f, 1.0f);
	// drawPlane(x, y);//1 to -0.5
	//glPopMatrix();
}


GLfloat TranslatePlaneX(GLfloat n)
{
	GLfloat min_x = -1;
	GLfloat max_x = 0.9;
	GLfloat min_t = 0.005;
	GLfloat max_t = 5;//will increase steps

	return ((n - 0.005) / (max_t - min_t)) * (max_x - min_x) + min_x;
}
