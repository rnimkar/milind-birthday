/**
 * @file linux_main.cpp
 * @brief entry point for the linux system
 *
 * @date 2023-08-04
 */

#include <cstdint>
#include <iostream>

#include <GL/gl.h>
#include <GL/glext.h>
#include <GL/glx.h>

#include "X11/XKBlib.h"
#include "X11/Xlib.h"
#include "geometry.h"
#include "main.h"
#include "plane.h"
#include <X11/X.h>
#include <X11/Xutil.h>
#include <X11/keysymdef.h>

#define GLX_MAJOR_MIN 1
#define GLX_MINOR_MIN 2

int main()
{
    Display*             dpy              = nullptr;
    Window               root             = 0UL;
    Window               w                = 0UL;
    int                  scr              = 0;
    GLint                glxMajor         = 0;
    GLint                glxMinor         = 0;
    XVisualInfo*         vi               = nullptr;
    XSetWindowAttributes xattr            = {};
    bool                 globalAbortFlag  = false;
    static Atom          wm_delete_window = 0;
    GLXContext           ctxt             = nullptr;
    int                  screenWidth      = 0;
    int                  screenHeight     = 0;

    dpy = XOpenDisplay(NULL);
    if (!glXQueryVersion(dpy, &glxMajor, &glxMinor))
    {
        std::cerr << "Failed to query glx version\n";
        exit(EXIT_FAILURE);
    }
    if (glxMajor < GLX_MAJOR_MIN && glxMinor < GLX_MINOR_MIN)
    {
        std::cerr << "GLX version >=1.2 is required\n";
        exit(EXIT_FAILURE);
    }
    std::cout << "glx version is " << glxMajor << "." << glxMinor << std::endl;

    screenWidth  = XDisplayWidth(dpy, XDefaultScreen(dpy));
    screenHeight = XDisplayHeight(dpy, XDefaultScreen(dpy));
    scr          = DefaultScreen(dpy);
    root         = XDefaultRootWindow(dpy);

    // clang-format off
    GLint glxAttriutes[] = {
        GLX_RGBA,
        GLX_DOUBLEBUFFER,
        GLX_DEPTH_SIZE, 24,
        GLX_STENCIL_SIZE, 8,
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE, 8,
        GLX_BLUE_SIZE, 8,
        GLX_SAMPLE_BUFFERS, 0,
        GLX_SAMPLES, 0,
        None
    };
    // clang-format on

    vi = glXChooseVisual(dpy, scr, glxAttriutes);
    if (nullptr == vi)
    {
        std::cerr << "Could not create required visual window\n";
        exit(EXIT_FAILURE);
    }
    xattr.border_pixel      = BlackPixel(dpy, scr);
    xattr.background_pixel  = WhitePixel(dpy, scr);
    xattr.override_redirect = true;
    xattr.colormap          = XCreateColormap(dpy, root, vi->visual, AllocNone);
    xattr.event_mask        = ExposureMask | KeyPressMask | StructureNotifyMask;

    w = XCreateWindow(dpy, root, 0, 0, 1024, 1024, 0, vi->depth, InputOutput, vi->visual, CWBackPixel | CWColormap | CWBorderPixel | CWEventMask, &xattr);
    XStoreName(dpy, w, "Happy Birthday Milind");
    XWindowAttributes attributes = {};
    XGetWindowAttributes(dpy, w, &attributes);
    XMoveResizeWindow(dpy, w, attributes.x, attributes.y, screenWidth, screenHeight);
    ctxt = glXCreateContext(dpy, vi, nullptr, GL_TRUE);
    glXMakeCurrent(dpy, w, ctxt);

    /* free XVisual as it is not required */
    XFree(vi);
    vi = nullptr;

    wm_delete_window = XInternAtom(dpy, "WM_DELETE_WINDOW", False);
    XSetWMProtocols(dpy, w, &wm_delete_window, 1);
    XMapWindow(dpy, w);

    while (!globalAbortFlag)
    {
        XEvent evt;
        if(XPending(dpy))
        {
            XNextEvent(dpy, &evt);
        switch (evt.type)
        {
        case Expose:
        {
            std::cout << "Expose" << std::endl;

            break;
        }
        case ClientMessage:
        {
            std::cout << "Client message\n";
            if (evt.xclient.data.l[0] == wm_delete_window) { globalAbortFlag = true; }

            break;
        }
        case KeyPress:
        {
            KeySym sym = XkbKeycodeToKeysym(dpy, evt.xkey.keycode, 0, 0);
            if (XK_Escape == sym)
            {
                globalAbortFlag = true;
                std::cout << "Escape pressed terminating\n";
            }
            break;
        }
        case MapNotify:
        {
            std::cout << "GL Vendor: " << glGetString(GL_VENDOR) << "\n";
            std::cout << "GL Renderer: " << glGetString(GL_RENDERER) << "\n";
            std::cout << "GL Version: " << glGetString(GL_VERSION) << "\n";
            std::cout << "GL Shading Language: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << "\n";
            break;
        }
        default:
        {
            std::cout << "Default event: " << evt.type << std::endl;
            break;
        }
        }
        }
        static double counter = .0;
        /* redraw frame */
        glClear(GL_COLOR_BUFFER_BIT);
        // displayGrid();
        std::cout << "redrawing" << std::endl;
        counter += 0.001;
        glPushMatrix();
        glColor3f(1.0f, 0.0f, 0.0f);
        glTranslatef(-counter, 0.0f, .0f);
        drawPlane({ 0.0f, 0.0f, 0.0f }, 1.0f);
        glPopMatrix();
        
        glXSwapBuffers(dpy, w);
    }

    /* resource cleanup */
    glXDestroyContext(dpy, ctxt);
    XFreeColormap(dpy, xattr.colormap);
    XDestroyWindow(dpy, w);
    XCloseDisplay(dpy);
    dpy = nullptr;
    return (0);
    
}
