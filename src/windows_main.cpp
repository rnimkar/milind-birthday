// common header files

#include "main.h"
#include "OGL.h"
#include <stdio.h>  // For file io
#include <stdlib.h> // for exit
#include <iostream>
#include <strsafe.h>
// Opengl Header files
// #include <gl/GL.h>
#define TIPPING_POINT_X 0.1
// #include "main.h"
#include "geometry.h"
#include "TowerAndPerson.h"
#include "plane.h"
#include "journey.h"
#include "ShaniwarWada.h"
#include "smartboard.h"
#include<Mmsystem.h>

// link with opengl libaray
#pragma comment(lib, "OpenGL32.lib")

// macros
#define WIN_WIDTH 800
#define WIN_HIGHT 600
// link with opengl libaray

// Opengl related variable
HDC   ghdc = NULL;
HGLRC ghrc = NULL;
// global function declartion
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
extern bool      g_bstartNight;
extern int       g_itime;
static double    g_counter = 0;//change values for different scenes
GLuint           gnFontList;
GLvoid*          list = "Happy Birthday Milind!";
bool g_bSoundflag = true;
// global variable declartion
FILE* gpFILE = NULL;
int   SH, SW;
int   WindowHeight = 600;
int   WindowWidth  = 800;
HWND  ghwnd        = NULL;
BOOL  gbActive     = FALSE;

DWORD           dwStyle      = 0;
WINDOWPLACEMENT wpPre        = { sizeof(WINDOWPLACEMENT) };
BOOL            gbFullscreen = FALSE;

// Entry Point Function

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{ // function declartions
    int  initialize(void);
    void uninitialize(void);
    void display(void);
    void update(void);

    // local variable declartions
    WNDCLASSEX wndclass;
    HWND       hwnd;
    MSG        msg;
    TCHAR      szAppName[] = TEXT("APSWindow");
    int        iResult     = 0;
    BOOL       bdone       = FALSE;

    // code
    // code
    gpFILE = fopen("Log.txt", "w");
    if (gpFILE == NULL)
    {
        MessageBox(NULL, TEXT("LOGFILE CAN NOT OPEN"), TEXT("ERROR"), MB_OK | MB_ICONERROR);
        exit(0);
    }
    fprintf(gpFILE, "Program Started Successfully\n");
    // wndclassex initization
    wndclass.cbSize        = sizeof(WNDCLASSEX);
    wndclass.style         = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wndclass.cbClsExtra    = 0;
    wndclass.cbWndExtra    = 0;
    wndclass.lpfnWndProc   = WndProc;
    wndclass.hInstance     = hInstance;
    wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wndclass.hIcon         = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
    wndclass.hCursor       = LoadCursor(NULL, IDC_ARROW);
    wndclass.lpszClassName = szAppName;
    wndclass.lpszMenuName  = NULL;
    wndclass.hIconSm       = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
    // get center of screen
    SH = GetSystemMetrics(SM_CYSCREEN);
    SW = GetSystemMetrics(SM_CXSCREEN);

    // register wndclassex
    RegisterClassEx(&wndclass);

    // create window
    hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName, TEXT("Happy BirthDay Milind"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, (SW / 2) - (WIN_WIDTH / 2),
        (SH / 2) - (WIN_HIGHT / 2), WIN_WIDTH, WIN_HIGHT, NULL, NULL, hInstance, NULL);

    ghwnd = hwnd;
    // initializetion
    iResult = initialize();
    if (iResult != 0)
    {
        MessageBox(hwnd, TEXT(" initialize() Fail"), TEXT("ERROR"), MB_OK | MB_ICONERROR);
        DestroyWindow(hwnd);
    }
    // show the window
    ShowWindow(hwnd, iCmdShow);

    SetForegroundWindow(hwnd);
    SetFocus(hwnd);

    // Game loop
    while (bdone == FALSE)
    {
        
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if (msg.message == WM_QUIT)
                bdone = TRUE;
            else
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }
        else
        {
            if (gbActive == TRUE)
            {
                // render
                display();

                // update
                update();
            }
        }
    }

    // uninitialize
    uninitialize();

    // message loop
    /*while (GetMessage(&msg, NULL, 0, 0))
    {

       TranslateMessage(&msg);
       DispatchMessage(&msg);

    }*/

    return ((int)msg.wParam);
}

// callback function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)

{ // function dec
    void ToggleFullScreen(void);
    void resize(int, int);
    // code

    switch (iMsg)
    {
    case WM_SETFOCUS: gbActive = TRUE; break;
    case WM_KILLFOCUS: gbActive = FALSE; break;
    case WM_SIZE: resize(LOWORD(lParam), HIWORD(lParam)); break;
    case WM_ERASEBKGND: return (0); break;
    case WM_KEYDOWN:
        switch (LOWORD(wParam))
        {
        case VK_ESCAPE: DestroyWindow(hwnd); break;
        }
        break;
    case WM_CHAR:
        switch (LOWORD(wParam))
        {
        case 'F':
        case 'f':
            if (gbFullscreen == FALSE)
            {
                ToggleFullScreen();
                gbFullscreen = TRUE;
                if (gpFILE) { fprintf(gpFILE, "Program FullScreen Successfully\n"); }
            }
            else
            {
                ToggleFullScreen();
                gbFullscreen = FALSE;
                if (gpFILE) { fprintf(gpFILE, "Program Normal size Successfully\n"); }
            }
            break;
        }
        break;
    case WM_CLOSE: DestroyWindow(hwnd); break;

    case WM_DESTROY: PostQuitMessage(0); break;
    default: break;
    }
    return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}
void ToggleFullScreen(void)
{
    // local variable declartion
    MONITORINFO mi = { sizeof(MONITORINFO) };
    // code
    if (gbFullscreen == FALSE)
    {
        dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
        if (dwStyle & WS_OVERLAPPEDWINDOW)
        {
            if (GetWindowPlacement(ghwnd, &wpPre) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
            {
                SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
                SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
            }
        }
        ShowCursor(FALSE);
    }
    else
    {
        SetWindowPlacement(ghwnd, &wpPre);
        SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
        ShowCursor(TRUE);
    }
}
int initialize(void)
{
    // void ToggleFullScreen(void);
    // ToggleFullScreen();
    // function declartions

    PIXELFORMATDESCRIPTOR pfd;
    int                   iPixcelFormatIndex = 0;

    GLYPHMETRICSFLOAT agmf[128];
    HFONT             hFont;
    LOGFONT           logFont;
    HRESULT           hr;

    ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
    // initializtion of pixcel format descriptor
    pfd.nSize           = sizeof(PIXELFORMATDESCRIPTOR); // size of the
    pfd.nVersion        = 1;                             // opengl version
    pfd.dwFlags         = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType      = PFD_TYPE_RGBA;
    pfd.cColorBits      = 32;
    pfd.cAccumRedBits   = 8;
    pfd.cAccumGreenBits = 8;
    pfd.cAccumBlueBits  = 8;
    pfd.cAlphaBits      = 8;
    ghdc                = GetDC(ghwnd);
    if (ghdc == NULL)
    {
        fprintf(gpFILE, "getdc Fail\n");

        return (-1);
    }

    iPixcelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

    if (iPixcelFormatIndex == 0)
    {
        fprintf(gpFILE, "Chose PixcelFormat Fail\n");
        return (-2);
    }
    if (SetPixelFormat(ghdc, iPixcelFormatIndex, &pfd) == FALSE)
    {
        fprintf(gpFILE, "SetPixelFormat Fail\n");
        return (-3);
    }
    // tell bridging lib fron this device context
    ghrc = wglCreateContext(ghdc);
    if (ghrc == NULL)
    {
        fprintf(gpFILE, "wglCreateContext Fail\n");
        return (-4);
    }
    // make rendering context current
    if (wglMakeCurrent(ghdc, ghrc) == FALSE)
    {
        fprintf(gpFILE, "wglMakeCurrent Fail\n");
        return (-5);
    }

    // set color of window to blue
    // glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    // SHREE GANESHA OF OPENGL

    // Amey
    // Setting the Font related attributes for the Font rendering from here
    // Set the Log font attributes
    logFont.lfHeight      = -15;
    logFont.lfWidth       = 3;
    logFont.lfEscapement  = 0;
    logFont.lfOrientation = 0;
    logFont.lfWeight      = FW_BOLD;
    logFont.lfItalic      = FALSE;

    logFont.lfUnderline      = FALSE;
    logFont.lfStrikeOut      = FALSE;
    logFont.lfCharSet        = ANSI_CHARSET;
    logFont.lfOutPrecision   = OUT_DEFAULT_PRECIS;
    logFont.lfClipPrecision  = CLIP_DEFAULT_PRECIS;
    logFont.lfQuality        = DEFAULT_QUALITY;
    logFont.lfPitchAndFamily = DEFAULT_PITCH;

    // strcpy(logFont.lfFaceName, "Arial");
    hr = StringCchCopy((STRSAFE_LPSTR)logFont.lfFaceName, LF_FACESIZE, (STRSAFE_LPSTR) "Wingdings");
    if (FAILED(hr))
    {
        fprintf(gpFILE, "Unable to Get the Charset");
        hr                = StringCchCopy((STRSAFE_LPSTR)logFont.lfFaceName, LF_FACESIZE, (STRSAFE_LPSTR) "Lucida Handwriting");
        logFont.lfCharSet = DEFAULT_CHARSET;
    }

    // Get the font handle
    hFont = CreateFontIndirect(&logFont);

    SelectObject(ghdc, hFont);

    // gnFontList = glGenLists(256);
    gnFontList = glGenLists(128);

    // This is the key function to create your 3D character Set
    wglUseFontOutlines(ghdc, 0, 128, gnFontList, 0.0f, 0.0f, WGL_FONT_POLYGONS, agmf);

    DeleteObject(hFont);

    return 0;
}
void resize(int width, int height)
{
    // code
    if (height <= 0) height = 1;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glViewport(0, 0, (GLsizei)width, (GLsizei)height); // viewport meaning binocular | bioscope
}
void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT);
    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    void screenbackground(Point pCenter);
    void drawcloud(Point pCenter);
    void drawtower(Point pCenter);
    void grassbackground(Point pCenter);
    void drawPerson(Point pCenter);

    Point cloudc = { 0.0, 0.0, 0.0 };
    g_counter += 0.5;
    if(g_counter == 360|| g_counter == 1450 || g_counter == 2550)
    {
        g_bSoundflag = true;
    }

    fprintf(gpFILE, "g_counter = %lf\n", g_counter);

if(g_counter < 360)//scene 0
{
    void PlayBell();
        PlayBell();
        screenbackground(cloudc);
        drawcloud(cloudc); 
        drawtower(cloudc);
        grassbackground(cloudc);
        Point person = { 0.25, -0.35, 0.0 };
        drawPerson(person); 
}
 else if (g_counter < 620) // 1st scene g_counter < 1000 //g_counter < 600
    {          // 2000   g_counter < 100
    void PlaySaareJahaSeAcha();
    PlaySaareJahaSeAcha();
        screenbackground(cloudc);
        drawcloud(cloudc); // cloud

        // Tower
        drawtower(cloudc);
        void drawRunway();
        drawRunway();
        // grassbackground(cloudc);//run way

        static double counter   = -1.8; // rotate scale translate
        static double planeTilt = 0.0;
        // if(!g_bBellflag){
            counter += 0.005; // 0.001
        if (planeTilt < 35 && counter > -0.5) planeTilt += 0.15;
        glPushMatrix();
        glColor3f(1.0f, 0.0f, 0.0f);
        // glRotatef(35,0.0f,0.0f,1.0f);//tilt upwards
        glRotatef(planeTilt, 0.0f, 0.0f, 1.0f);
        glRotatef(180, 0.0f, 1.0f, 0.0f);
        // glTranslatef(0, -0.65f, .0f);
        glTranslatef(-counter, -0.65f, .0f);

        drawPlane({ 0.0f, 0.0f, 0.0f }, 1.0f);

        glPushMatrix();//vistara
        VISTARA_PURPLE.print();
        glListBase(gnFontList);
        glRotatef(180, 0.0f, 1.0f, 0.0f);
        glTranslatef(0.2f, 0.0f, 0.0f);
        glScalef(0.12f, 0.08f, 0.0f);
        glCallLists(7, GL_UNSIGNED_BYTE, "vistaRa");
        glPopMatrix();

        glPopMatrix();
        
    }
    else if (g_counter < 1450) 
    {
        g_itime++;
        color_t SKY_COLORS[]{
  //  add colors
            {182, 216, 247}, //  light blue
            { 46, 68,  130}, //  dark blue
            { 19, 24,  98 }  //  darkest
        };
        drawsky(SKY_COLORS);

        //  std::cout<<g_counter;

        if (g_bstartNight)
        {
            drawStars();
            fprintf(gpFILE, "++++++night started = %lf\n", g_counter);
        }

        drawSun(0);
        drawsea();
        drawGermany(max((-g_itime * 0.005), (-2.2))); // 0 to -2.2

        static double counter = -1.5;
        counter += 0.0015;
        glPushMatrix();
        glColor3f(1.0f, 0.0f, 0.0f);
        // glRotatef(35,0.0f,0.0f,1.0f);//tilt upwards
        glRotatef(180, 0.0f, 1.0f, 0.0f);
        // glTranslatef(0, -0.65f, .0f);
        glTranslatef(-counter, 0.1f, .0f);

        drawPlane({ 0.0f, 0.0f, 0.0f }, 1.0f);

        glPushMatrix();//vistara
        VISTARA_PURPLE.print();
        glListBase(gnFontList);
        glRotatef(180, 0.0f, 1.0f, 0.0f);
        glTranslatef(0.2f, 0.0f, 0.0f);
        glScalef(0.12f, 0.08f, 0.0f);
        glCallLists(7, GL_UNSIGNED_BYTE, "vistaRa");
        glPopMatrix();

        glPopMatrix();

        // drawPlaneJourney(TranslatePlaneX(g_itime * 0.005), TranslatePlaneY((g_itime * 0.005), TranslatePlaneX(g_itime * 0.005) > TIPPING_POINT_X ? true : false));
    }
    else if (g_counter < 2350) 
    {
        void drawRunway();
        
        void PlayDhol();
        PlayDhol();
        displayGrid();
        glColor3f(1.0f, 0.0f, 0.0f);

        
        drawShaniwarWada();
        drawRunway();


        static double counter   = -1.5; // rotate scale translate
        static double planeTilt = -10;
        static float plane_y = -0.5f;
        if (counter < 1.3)//0.3
        {
            counter += 0.0015; // 0.001
        }
         if (planeTilt < 6 && counter > -1.2 && counter < -0.5) 
         {
            planeTilt += 0.05;
         }
        else if(planeTilt > 0 && counter > -0.5 && plane_y > -0.9)
            {
                planeTilt -= 0.05;
                plane_y -= 0.0005;
            }
        glPushMatrix();
        glColor3f(1.0f, 0.0f, 0.0f);
        // glRotatef(35,0.0f,0.0f,1.0f);//tilt upwards
        glRotatef(planeTilt, 0.0f, 0.0f, 1.0f);
        glRotatef(180, 0.0f, 1.0f, 0.0f);
        // glTranslatef(0, -0.65f, .0f);

        glTranslatef(-counter, plane_y, .0f);

        drawPlane({ 0.0f, 0.0f, 0.0f }, 1.0f);

        glPushMatrix();//vistara
        VISTARA_PURPLE.print();
        glListBase(gnFontList);
        glRotatef(180, 0.0f, 1.0f, 0.0f);
        glTranslatef(0.2f, 0.0f, 0.0f);
        glScalef(0.12f, 0.08f, 0.0f);
        glCallLists(7, GL_UNSIGNED_BYTE, "vistaRa");
        glPopMatrix();

        glPopMatrix();

    }
    else if(g_counter < 2550)
    {
        displayGrid();
        glColor3f(1.0f, 0.0f, 0.0f);

        drawShaniwarWada();
        drawPerson({ 0.32f, -0.3f, 0.0f });
    }
    else
    {

        void PlayLORsong();
        PlayLORsong();
        point_t RectOrigin = { 0.0f, 0.1f };
        
        color_t WALLCOLOR = {149,206,248};
         glClearColor(0.0f, 0.0f,0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

    WALLCOLOR.print();
    drawRectangle({0,0},2,2,1.0f);

        glPushMatrix();
        drawSmartBoard(RectOrigin, 3, 2.5, 0.5, DARK_GREY);
        YELLOW.print();
        glListBase(gnFontList);
        glTranslatef(-0.55f, 0.3f, 0.0f);
        glScalef(0.12f, 0.3f, 0.0f);
        glCallLists(22, GL_UNSIGNED_BYTE, "Happy Birthday Milind!");
         glPopMatrix();



         glPushMatrix();
        WHITE.print();
        glListBase(gnFontList);
        glTranslatef(-5.2f, -2.75f, 0.0f);
        glScalef(0.2f, 0.18f, 0.0f);
        glCallLists(3, GL_UNSIGNED_BYTE, "LG");
         glPopMatrix();
    }

    SwapBuffers(ghdc);
    // code
}
void update(void)
{
    // code
    Sleep(5);
}

void uninitialize(void)
{
    // function declartions
    void ToggleFullScreen(void);

    // code
    //  if application is exiting full screen that time below code work
    if (gbFullscreen == TRUE)
    {
        ToggleFullScreen();
        gbFullscreen = FALSE;
    }

    // make the hdc as current context
    if (wglGetCurrentContext() == ghrc) { wglMakeCurrent(NULL, NULL); }
    // delet rendering context
    if (ghrc)
    {
        wglDeleteContext(ghrc);
        ghrc = NULL;
    }
    // relese the hdc
    if (ghdc)
    {
        ReleaseDC(ghwnd, ghdc);

        ghdc = NULL;
    }

    // distory window
    if (ghwnd)
    {
        DestroyWindow(ghwnd);
        ghwnd = NULL;
    }

    // close log file
    if (gpFILE)
    {
        fprintf(gpFILE, "Program Ended Successfully\n");
        fclose(gpFILE);
        gpFILE = NULL;
    }
}

void PlayBell()
{
    if(g_bSoundflag)
    {
    PlaySound(TEXT("bell.wav"), NULL, SND_ASYNC|SND_FILENAME);
    g_bSoundflag = false;
    }
}

void PlaySaareJahaSeAcha()
{
    if(g_bSoundflag)
    {
    PlaySound(TEXT("sarejahaseacha.wav"), NULL, SND_ASYNC|SND_FILENAME);
    g_bSoundflag = false;
    }
}

void PlayDhol()
{
    if(g_bSoundflag)
    {
    PlaySound(TEXT("dhol.wav"), NULL, SND_ASYNC|SND_FILENAME);
    g_bSoundflag = false;
    }
}

void PlayLORsong()
{
    if(g_bSoundflag)
    {
    PlaySound(TEXT("lor.wav"), NULL, SND_ASYNC|SND_FILENAME);
    g_bSoundflag = false;
    }
}

void drawRunway()
{
        // Grey Rect
        glBegin(GL_QUADS);
        glColor3f(0.128,0.128,0.128);
        glVertex3f(-1.0f,-1.0f,0.0f);
        glVertex3f(1.0f,-1.0f,0.0f);
        glVertex3f(1.0f,-0.4f,0.0f);
        glVertex3f(-1.0f,-0.4f,0.0);
        glEnd();

        // top white strap
        glBegin(GL_QUADS);
        glColor3f(1,1,1);
        glVertex3f(-1.0, -0.4, 0.0);
        glVertex3f(-1.0, -0.42, 0.0);
        glVertex3f(1.0, -0.42, 0.0);
        glVertex3f(1.0, -0.4, 0.0);
        glEnd();

        //middle white strap 1
        glBegin(GL_QUADS);
        glColor3f(1,1,1);
        glVertex3f(-1.0f, -0.65f, 0.0f);
        glVertex3f(-1.0f, -0.71f, 0.0f);
        glVertex3f(-0.6f, -0.71f, 0.0f);
        glVertex3f(-0.6f, -0.65f, 0.0f);
        glEnd();

        // middle white strap 2
        glBegin(GL_QUADS);
        glColor3f(1,1,1);
        glVertex3f(-0.3f, -0.65f, 0.0f);
        glVertex3f(-0.3f, -0.71f, 0.0f);
        glVertex3f(0.2f, -0.71f, 0.0f);
        glVertex3f(0.2f, -0.65f, 0.0f);
        glEnd();

        // middle white strap 3
        glBegin(GL_QUADS);
        glColor3f(1,1,1);
        glVertex3f(0.5f, -0.65f, 0.0f);
        glVertex3f(0.5f, -0.71f, 0.0f);
        glVertex3f(0.9f, -0.71f, 0.0f);
        glVertex3f(0.9f, -0.65f, 0.0f);
         glEnd();

        // bottom white strap
        glBegin(GL_QUADS);
        glColor3f(1,1,1);
        glVertex3f(-1.0f, -1.0f, 0.0f);
        glVertex3f(-1.0f, -0.98f, 0.0f);
        glVertex3f(1.0f, -0.98f, 0.0f);
        glVertex3f(1.0f, -1.0f, 0.0f);
        glEnd();

}