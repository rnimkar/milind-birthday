#ifndef COMMON_H
#define COMMON_H

#include <cstdint>
inline float scaleColor(uint8_t val)
{
    return (float)val/255;
}


#endif // !COMMON_H
